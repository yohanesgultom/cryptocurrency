package me.gultom.cryptocurrency;

public interface WalletFactory {
    Wallet generate();
    Wallet generate(Network network);
    Wallet create(String publicAddress, String privateKey);
    Wallet create(String publicAddress, String privateKey, Network network);
}

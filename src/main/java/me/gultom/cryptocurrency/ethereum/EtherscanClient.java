package me.gultom.cryptocurrency.ethereum;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.squareup.okhttp.*;
import me.gultom.cryptocurrency.Network;
import me.gultom.cryptocurrency.Result;
import me.gultom.cryptocurrency.exception.APIErrorException;
import org.ethereum.core.Denomination;
import org.ethereum.core.Transaction;
import org.ethereum.crypto.ECKey;
import org.spongycastle.util.encoders.Hex;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class EtherscanClient {
    private final String MAIN_URL = "https://api.etherscan.io/api";
    private final String ROPSTEN_URL = "https://api-ropsten.etherscan.io/api";

    private Network network;
    private String token;
    private OkHttpClient httpClient;
    private ObjectMapper mapper;

    public EtherscanClient(Network network, String token) {
        this.network = network;
        this.token = token;
        this.httpClient = new OkHttpClient();
        this.mapper = new ObjectMapper();
    }

    private String getUrl() {
        String url;
        if (this.network.equals(Network.MAIN)) {
            url = MAIN_URL;
        } else { // TEST
            url = ROPSTEN_URL;
        }
        return url;
    }

    public Result getGasPrice() throws IOException {
        String url = this.getUrl();
        HttpUrl.Builder httpBuider = HttpUrl.parse(url).newBuilder();
        httpBuider.addQueryParameter("module", "proxy");
        httpBuider.addQueryParameter("action", "eth_gasPrice");
        httpBuider.addQueryParameter("apiKey", this.token);
        Request request = new Request.Builder().url(httpBuider.build()).get().build();
        Response response = this.httpClient.newCall(request).execute();
        String bodyString = response.body().string();
        JsonNode body = mapper.readTree(bodyString);

        if (body.has("error")) {
            List<String> errors = new ArrayList<>();
            if (body.get("error").isArray()) {
                for (final JsonNode node : body.get("error")) {
                    errors.add(node.textValue());
                }
            } else {
                errors.add(body.get("error").textValue());
            }
            return new Result(false, errors.toArray(new String[0]), 500);
        }

        return new Result(body);
    }

    public Result getEstimateGas(String addressTo, BigInteger weiAmount, String gasPriceHex)
            throws IOException {

        if (!addressTo.toLowerCase().startsWith("0x")) {
            addressTo = "0x" + addressTo;
        }

        String value = Hex.toHexString(weiAmount.toByteArray());
        String url = this.getUrl();
        HttpUrl.Builder httpBuider = HttpUrl.parse(url).newBuilder()
                .addQueryParameter("module", "proxy")
                .addQueryParameter("action", "eth_estimateGas")
                .addQueryParameter("apiKey", this.token)
                .addQueryParameter("to", addressTo)
                .addQueryParameter("value", value)
                .addQueryParameter("gasPrice", gasPriceHex)
                .addQueryParameter("gas", "0xffffff");
        Request request = new Request.Builder().url(httpBuider.build()).get().build();
        Response response = this.httpClient.newCall(request).execute();
        String bodyString = response.body().string();
        JsonNode body = mapper.readTree(bodyString);

        if (body.has("error")) {
            List<String> errors = new ArrayList<>();
            if (body.get("error").isArray()) {
                for (final JsonNode node : body.get("error")) {
                    errors.add(node.textValue());
                }
            } else {
                errors.add(body.get("error").textValue());
            }
            return new Result(false, errors.toArray(new String[0]), 500);
        }

        return new Result(body);
    }

    public Result  getTransactionCount(String address)
            throws IOException {

        if (!address.toLowerCase().startsWith("0x")) {
            address = "0x" + address;
        }

        String url = this.getUrl();
        HttpUrl.Builder httpBuider = HttpUrl.parse(url).newBuilder()
                .addQueryParameter("module", "proxy")
                .addQueryParameter("action", "eth_getTransactionCount")
                .addQueryParameter("apiKey", this.token)
                .addQueryParameter("address", address)
                .addQueryParameter("tag", "latest");
        Request request = new Request.Builder().url(httpBuider.build()).get().build();
        Response response = this.httpClient.newCall(request).execute();
        String bodyString = response.body().string();
        JsonNode body = mapper.readTree(bodyString);

        if (body.has("error")) {
            List<String> errors = new ArrayList<>();
            if (body.get("error").isArray()) {
                for (final JsonNode node : body.get("error")) {
                    errors.add(node.textValue());
                }
            } else {
                errors.add(body.get("error").textValue());
            }
            return new Result(false, errors.toArray(new String[0]), 500);
        }

        return new Result(body);

    }

    public BigDecimal getBalance(String address) throws IOException, APIErrorException {
        String url = this.getUrl();
        HttpUrl.Builder httpBuider = HttpUrl.parse(url).newBuilder()
                .addQueryParameter("module", "account")
                .addQueryParameter("action", "balance")
                .addQueryParameter("apiKey", this.token)
                .addQueryParameter("address", address)
                .addQueryParameter("tag", "latest");
        Request request = new Request.Builder().url(httpBuider.build()).get().build();
        Response response = this.httpClient.newCall(request).execute();
        JsonNode node = mapper.readTree(response.body().string());
        String status = node.get("status").textValue();
        String message = node.get("message").textValue();
        if (!"1".equalsIgnoreCase(status) || !"OK".equalsIgnoreCase(message)) {
            throw new APIErrorException(node.get("result").textValue());
        }
        BigInteger balanceInWei = new BigInteger(node.get("result").textValue());
        return new BigDecimal(balanceInWei).divide(new BigDecimal(Denomination.ETHER.value()));
    }

    public Result send(BigDecimal ethAmount, String privateKey, String addressTo)
            throws IOException {

        ECKey ecKey = ECKey.fromPrivate(Hex.decode(privateKey));
        BigInteger weiAmount = ethAmount.multiply(new BigDecimal(Denomination.ETHER.value())).toBigInteger();
        Result result;

        result = this.getGasPrice();
        if (!result.isSuccess()){
            return result;
        }
        String gasPriceHex = result.getBody().get("result").textValue();

        result = this.getEstimateGas(addressTo, weiAmount, gasPriceHex);
        if (!result.isSuccess()){
            return result;
        }
        String gasHex = result.getBody().get("result").textValue();

        result = this.getTransactionCount("0x" + Hex.toHexString(ecKey.getAddress()));
        if (!result.isSuccess()){
            return result;
        }
        String countHex = result.getBody().get("result").textValue();

        if (addressTo.toLowerCase().startsWith("0x")) {
            addressTo = addressTo.substring(2);
        }

        byte[] to = Hex.decode(addressTo);
        byte[] gasPrice = new BigInteger(gasPriceHex.substring(2), 16).toByteArray();
        byte[] value = weiAmount.toByteArray();
        byte[] gasLimit = new BigInteger(gasHex.substring(2), 16).toByteArray();
        byte[] nonce = new BigInteger(countHex.substring(2), 16).toByteArray();

        Transaction tx = new Transaction(nonce, gasPrice, gasLimit, to, value, null);
        tx.getHash();
        tx.sign(ecKey);
        String signedTx = Hex.toHexString(tx.getEncoded());
        RequestBody requestBody = new FormEncodingBuilder()
                .add("module", "proxy")
                .add("action", "eth_sendRawTransaction")
                .add("hex", signedTx)
                .add("apikey", this.token)
                .build();

        String url = this.getUrl();
        Request request = new Request.Builder().url(url).post(requestBody).build();
        Response response = this.httpClient.newCall(request).execute();

        if (!response.isSuccessful()) {
            return new Result(false, new String[]{ String.format("%s %s", response.code(), response.message()) }, response.code());
        }

        JsonNode body = mapper.readTree(response.body().string());
        if (body.has("error") && body.get("error") != null) {
            return new Result(false, new String[]{ body.get("error").get("message").textValue() }, response.code());
        }

        return new Result(body);
    }

//    public String signTransaction(BigDecimal ethAmount, String privateKey, String addressTo) throws IOException {
//        ECKey ecKey = ECKey.fromPrivate(Hex.decode(privateKey));
//        BigInteger weiAmount = ethAmount.multiply(new BigDecimal(Denomination.ETHER.value())).toBigInteger();
//
//        String gasPriceHex = this.getGasPrice();
//        String gasHex = this.getEstimateGas(addressTo, weiAmount, gasPriceHex);
//        String countHex = this.getTransactionCount("0x" + Hex.toHexString(ecKey.getAddress()));
//
//        if (addressTo.toLowerCase().startsWith("0x")) {
//            addressTo = addressTo.substring(2);
//        }
//        byte[] to = Hex.decode(addressTo);
//        byte[] gasPrice = new BigInteger(gasPriceHex.substring(2), 16).toByteArray();
//        byte[] value = weiAmount.toByteArray();
//        byte[] gasLimit = new BigInteger(gasHex.substring(2), 16).toByteArray();
//        byte[] nonce = new BigInteger(countHex.substring(2), 16).toByteArray();
//
//        Transaction tx = new Transaction(nonce, gasPrice, gasLimit, to, value, null);
//        tx.sign(ecKey);
//        String signedTx = Hex.toHexString(tx.getEncoded());
//        RequestBody requestBody = new FormEncodingBuilder()
//                .add("module", "proxy")
//                .add("action", "eth_sendRawTransaction")
//                .add("hex", signedTx)
//                .add("apikey", this.token)
//                .build();
//        return requestBody.toString();
//    }

    public Result createTransactionHash(BigDecimal ethAmount, String address, String addressTo) throws IOException{

        if (address.toLowerCase().startsWith("0x")) {
            address = addressTo.substring(2);
        }

        if (addressTo.toLowerCase().startsWith("0x")) {
            addressTo = addressTo.substring(2);
        }

        BigInteger weiAmount = ethAmount.multiply(new BigDecimal(Denomination.ETHER.value())).toBigInteger();
        Result result;

        result = this.getGasPrice();
        if (!result.isSuccess()){
            return result;
        }
        String gasPriceHex = result.getBody().get("result").textValue();
        System.out.println("###GasPriceHex = "+gasPriceHex);

        result = this.getEstimateGas(addressTo, weiAmount, gasPriceHex);
        System.out.println("###CODE = "+result.getCode());
        System.out.println("###ISSUCCESS = "+result.isSuccess());
        System.out.println("###BODY = "+result.getBody());
        if (!result.isSuccess()){
            return result;
        }
        String gasHex = result.getBody().get("result").textValue();
        System.out.println("###GasPriceHex = "+gasHex);

        result = this.getTransactionCount(address);
        System.out.println("###CODE = "+result.getCode());
        System.out.println("###ISSUCCESS = "+result.isSuccess());
        System.out.println("###BODY = "+result.getBody());
        if (!result.isSuccess()){
            return result;
        }
        String countHex = result.getBody().get("result").textValue();
        System.out.println("###CountHex = "+gasHex);


        byte[] to = Hex.decode(addressTo);
        byte[] gasPrice = new BigInteger(gasPriceHex.substring(2), 16).toByteArray();
        byte[] value = weiAmount.toByteArray();
        byte[] gasLimit = new BigInteger(gasHex.substring(2), 16).toByteArray();
        byte[] nonce = new BigInteger(countHex.substring(2), 16).toByteArray();

        Transaction tx = new Transaction(nonce, gasPrice, gasLimit, to, value, null);
        tx.getHash();
//        tx.getEncoded();

        ArrayNode arrayNode = this.mapper.createArrayNode();
        arrayNode.add(Hex.toHexString(tx.getRawHash()));
        ObjectNode objectNode = this.mapper.createObjectNode();
        objectNode.set("tosign", arrayNode);

        System.out.println("#### RawHex : "+Hex.toHexString(tx.getRawHash()));

        JsonNode body = objectNode.deepCopy();
        result = new Result(body);

        System.out.println("RESULT : "+body);

        return result;
    }

    public Result sendTransaction(String signature) throws IOException{
        RequestBody requestBody = new FormEncodingBuilder()
                .add("module", "proxy")
                .add("action", "eth_sendRawTransaction")
                .add("hex", signature)
                .add("apikey", this.token)
                .build();

        String url = this.getUrl();
        Request request = new Request.Builder().url(url).post(requestBody).build();
        Response response = this.httpClient.newCall(request).execute();

        String bodyString = response.body().string();
        JsonNode body = mapper.readTree(bodyString);

        System.out.println("#### ResponseTransaction : "+bodyString);

        if (body.has("error")) {
            System.out.println("ERROR");
            List<String> errors = new ArrayList<>();
            if (body.get("error").isArray()) {
                for (final JsonNode node : body.get("error")) {
                    errors.add(node.textValue());
                }
            } else {
                errors.add(body.get("error").textValue());
            }
            return new Result(false, errors.toArray(new String[0]), 500);
        }

        System.out.println("GA ERROR");

        System.out.println("### BODY : "+body.textValue());

        if (body.has("error") && body.get("error") != null) {
            return new Result(false, new String[]{ body.get("error").get("message").textValue() }, response.code());
        }

        return new Result(body);
    }

}

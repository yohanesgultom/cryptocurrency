package me.gultom.cryptocurrency.ethereum;

import me.gultom.cryptocurrency.HDWallet;
import me.gultom.cryptocurrency.Network;
import org.web3j.crypto.Bip39Wallet;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.WalletUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class EthereumHDWalletFactory {
    public HDWallet generate() throws IOException, CipherException {
        return this.generate(Network.MAIN);
    }

    public HDWallet generate(Network network) throws IOException, CipherException {
        File tempDir = Files.createTempDirectory(null).toFile();
        Bip39Wallet wallet = WalletUtils.generateBip39Wallet("", tempDir);
        return new EthereumHDWallet(wallet);
    }

    public HDWallet create(String mnemonicCode) throws Exception {
        return this.create(mnemonicCode, Network.MAIN);
    }

    public HDWallet create(String mnemonicCode, Network network) throws Exception {
        File tempFile = Files.createTempFile(null, null).toFile();
        Bip39Wallet wallet = new Bip39Wallet(tempFile.getName(), mnemonicCode);
        return new EthereumHDWallet(wallet, network);
    }

}

package me.gultom.cryptocurrency.ethereum;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.BigIntegerNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import me.gultom.cryptocurrency.Network;
import me.gultom.cryptocurrency.Result;
import me.gultom.cryptocurrency.exception.APIErrorException;
import org.ethereum.core.Denomination;
import org.ethereum.crypto.ECKey;
import org.spongycastle.util.encoders.Hex;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * This is the client for some APIs in https://www.blockcypher.com/dev/ethereum
 *
 * IMPORTANT:
 * Unlike for BTC, BlockCypher does not use public tesnet network for ETH.
 * To add ETH in a test wallet/account, a faucet API is provided (can only send max. 1 ETH per request)
 * More details described at https://www.blockcypher.com/dev/ethereum/#testing
 *
 */
public class BlockCypherClient extends me.gultom.cryptocurrency.apiclient.BlockCypherClient {

	// Supported event types https://www.blockcypher.com/dev/ethereum/#types-of-events
	public interface Event {
		// callback payload is tx https://www.blockcypher.com/dev/ethereum/#tx
		String UNCONFIRMEDTX = "unconfirmed-tx";
		String CONFIRMEDTX = "confirmed-tx";
	}

	private final String MAIN_URL = "https://api.blockcypher.com/v1/eth/main";
	private final String TESTNET_URL = "https://api.blockcypher.com/v1/beth/test";

	public BlockCypherClient(Network network, String token) {
		super(network, token);
	}

	protected String getUrl() {
		String url;
		if (this.network.equals(Network.MAIN)) {
			url = MAIN_URL;
		} else { // TEST
			url = TESTNET_URL;
		}
		return url;
	}

	/**
	 * Get (confirmed) balance in Wei and return as ETH
	 * @param address
	 * @return BigDecimal ETH amount
	 * @throws IOException
	 * @throws APIErrorException
	 */
	public BigDecimal getBalance(String address) throws IOException, APIErrorException {
		if (address.toLowerCase().startsWith("0x")) {
			address = address.substring(2);
		}
		String url = String.format("%s/addrs/%s/balance", this.getUrl(), address);
		Request request = new Request.Builder().url(url).get().build();
		Response response = this.httpClient.newCall(request).execute();
		String bodyString = response.body().string();
		if (!response.isSuccessful()) {
			throw new APIErrorException(String.format("%s %s", response.code(), response.message()));
		} else {
			JsonNode body = mapper.readTree(bodyString);
			if (!body.has("balance")) {
				throw new APIErrorException(String.format("Unexpected response: %s", bodyString));
			} else {
				BigInteger balanceInWei = new BigInteger(body.get("balance").asText());
				return new BigDecimal(balanceInWei).divide(new BigDecimal(Denomination.ETHER.value()));
			}
		}
	}

	/**
	 * Send ETH amount using two-endpoint transaction creation tool
	 * https://www.blockcypher.com/dev/ethereum/#creating-transactions
	 * @param ethAmount
	 * @param privateKey
	 * @param addressTo
	 * @return Result API result
	 * @throws IOException
	 */
	public Result send(BigDecimal ethAmount, String privateKey, String addressTo) throws IOException {
		ECKey ecKey = ECKey.fromPrivate(Hex.decode(privateKey));
		BigInteger weiAmount = ethAmount.multiply(new BigDecimal(Denomination.ETHER.value())).toBigInteger();
		String addressFrom = Hex.toHexString(ecKey.getAddress());

		// inputs
		ArrayNode inputs = this.mapper.createArrayNode();
		ObjectNode input = this.mapper.createObjectNode();
		ArrayNode inputAddresses = this.mapper.createArrayNode();
		inputAddresses.add(addressFrom);
		input.set("addresses", inputAddresses);
		inputs.add(input);

		// outputs
		if (addressTo.toLowerCase().startsWith("0x")) {
			addressTo = addressTo.substring(2);
		}
		ArrayNode outputs = this.mapper.createArrayNode();
		ObjectNode output = this.mapper.createObjectNode();
		ArrayNode outputAddresses = this.mapper.createArrayNode();
		outputAddresses.add(addressTo);
		output.set("addresses", outputAddresses);
		output.set("value", new BigIntegerNode(weiAmount));
		outputs.add(output);

		ObjectNode payload = this.mapper.createObjectNode();
		payload.set("inputs", inputs);
		payload.set("outputs", outputs);

		// get transaction skeleton
		String url = this.getUrl() + "/txs/new?token=" + this.token;
		Response response = this._post(payload, url, JSON);
		String bodyString = response.body().string();
		if (!response.isSuccessful()) {
			return this._buildResult(false, response.code(), response.message(), bodyString);
		}

		JsonNode body = mapper.readTree(bodyString);
		if (body.has("errors")) {
			List<String> errors = new ArrayList<>();
			if (body.get("errors").isArray()) {
				for (final JsonNode node : body.get("errors")) {
					errors.add(node.textValue());
				}
			} else {
				errors.add(body.get("errors").textValue());
			}
			return new Result(false, errors.toArray(new String[0]), 500);
		}

		// sign transaction
		ArrayNode signatures = this.mapper.createArrayNode();
		for (JsonNode node : body.get("tosign")) {
			String msg = node.asText().trim();
			ECKey.ECDSASignature sig = ecKey.sign(Hex.decode(msg));
			signatures.add(sig.toHex());
		}
		payload = body.deepCopy();
		payload.set("signatures", signatures);

		// submit signed transaction
		url = this.getUrl() + "/txs/send?token=" + this.token;
		response = this._post(payload, url, TEXT);
		return this._buildResult(response);
	}

	/**
	 * Register webhook using non-formatted ETH address
	 * @param eventType
	 * @param address
	 * @param callbackURL
	 * @return Result API result
	 * @throws IOException
	 */
	public Result registerWebhook(String eventType, String address, String callbackURL) throws IOException {
		if (address.toLowerCase().startsWith("0x")) {
			address = address.substring(2);
		}
		return super.registerWebhook(eventType, address, callbackURL);
	}

	/**
	 * Example of how to extract received amount from webhook TX payload
	 * @param json TX JSON string https://www.blockcypher.com/dev/ethereum/#tx
	 * @param address Receiving (output) address
	 * @return BigDecimal Received amount in ETH
	 * @throws IOException
	 */
	public BigDecimal getReceivedAmountFromTX(String json, String address) throws IOException {
		BigDecimal amount = null;
		JsonNode tx = mapper.readTree(json);
		if (!tx.hasNonNull("outputs") || !tx.get("outputs").isArray()) {
			throw new IOException("No outputs field found");
		}
		if (address.toLowerCase().startsWith("0x")) {
			address = address.substring(2);
		}
		boolean found = false;
		for (JsonNode output:tx.get("outputs")) {
			if (output.hasNonNull("addresses") && output.get("addresses").isArray()) {
				for (JsonNode outAddress:output.get("addresses")) {
					if (address.equalsIgnoreCase(outAddress.asText())) {
						if (!output.hasNonNull("value")) {
							throw new IOException("Output address found but no value field found");
						}
						BigInteger balanceInWei = new BigInteger(output.get("value").asText());
						amount = new BigDecimal(balanceInWei).divide(new BigDecimal(Denomination.ETHER.value()));
						found = true;
						break;
					}
				}
			}
			if (found) break;
		}
		return amount;
	}

	public BigDecimal getTransactionFee( BigDecimal ethAmount, String address, String addressTo) throws  IOException{

		BigInteger weiAmount = ethAmount.multiply(new BigDecimal(Denomination.ETHER.value())).toBigInteger();

		// inputs
		ArrayNode inputs = this.mapper.createArrayNode();
		ObjectNode input = this.mapper.createObjectNode();
		ArrayNode inputAddresses = this.mapper.createArrayNode();
		inputAddresses.add(address);
		input.set("addresses", inputAddresses);
		inputs.add(input);

		// outputs
		if (addressTo.toLowerCase().startsWith("0x")) {
			addressTo = addressTo.substring(2);
		}
		ArrayNode outputs = this.mapper.createArrayNode();
		ObjectNode output = this.mapper.createObjectNode();
		ArrayNode outputAddresses = this.mapper.createArrayNode();
		outputAddresses.add(addressTo);
		output.set("addresses", outputAddresses);
		output.set("value", new BigIntegerNode(weiAmount));
		outputs.add(output);

		ObjectNode payload = this.mapper.createObjectNode();
		payload.set("inputs", inputs);
		payload.set("outputs", outputs);

//		System.out.println("### skeleton : "+payload.toString());

		// get transaction skeleton
		String url = this.getUrl() + "/txs/new?token=" + this.token;
		Response response = this._post(payload, url, JSON);


		String bodyString = response.body().string();

//		System.out.println("### url : "+url);
//		System.out.println("### bodystring : "+bodyString);


		JsonNode body = mapper.readTree(bodyString);
		JsonNode txBody = body.get("tx");

		BigInteger fees = new BigInteger(txBody.get("fees").asText());

		return new BigDecimal(fees).divide(new BigDecimal(Denomination.ETHER.value()));
	}


	public Result createAndSendSkeletonRequest( BigDecimal ethAmount, String address, String addressTo) throws  IOException{

		BigInteger weiAmount = ethAmount.multiply(new BigDecimal(Denomination.ETHER.value())).toBigInteger();

		// inputs
		ArrayNode inputs = this.mapper.createArrayNode();
		ObjectNode input = this.mapper.createObjectNode();
		ArrayNode inputAddresses = this.mapper.createArrayNode();
		inputAddresses.add(address);
		input.set("addresses", inputAddresses);
		inputs.add(input);

		// outputs
		if (addressTo.toLowerCase().startsWith("0x")) {
			addressTo = addressTo.substring(2);
		}
		ArrayNode outputs = this.mapper.createArrayNode();
		ObjectNode output = this.mapper.createObjectNode();
		ArrayNode outputAddresses = this.mapper.createArrayNode();
		outputAddresses.add(addressTo);
		output.set("addresses", outputAddresses);
		output.set("value", new BigIntegerNode(weiAmount));
		outputs.add(output);

		ObjectNode payload = this.mapper.createObjectNode();
		payload.set("inputs", inputs);
		payload.set("outputs", outputs);

//		System.out.println("### skeleton : "+payload.toString());

		// get transaction skeleton
		String url = this.getUrl() + "/txs/new?token=" + this.token;
		Response response = this._post(payload, url, JSON);


		String bodyString = response.body().string();

//		System.out.println("### url : "+url);
//		System.out.println("### bodystring : "+bodyString);

		if (!response.isSuccessful()) {
			return this._buildResult(false, response.code(), response.message(), bodyString);
		}

		JsonNode body = mapper.readTree(bodyString);
		if (body.has("errors")) {
			List<String> errors = new ArrayList<>();
			if (body.get("errors").isArray()) {
				for (final JsonNode node : body.get("errors")) {
					errors.add(node.toString());
				}
			} else {
				errors.add(body.get("errors").asText());
			}
			return new Result(false, errors.toArray(new String[0]), body, 500);
		}

		return new Result(body);
	}

	public Result createAndSendSkeletonRequest( BigDecimal ethAmount, BigDecimal fees,  String address, String addressTo) throws  IOException{

		BigInteger weiAmount = ethAmount.multiply(new BigDecimal(Denomination.ETHER.value())).toBigInteger();

//		System.out.println("ethAmount : "+ethAmount);
//		System.out.println("weiAmount : "+weiAmount);

		// inputs
		ArrayNode inputs = this.mapper.createArrayNode();
		ObjectNode input = this.mapper.createObjectNode();
		ArrayNode inputAddresses = this.mapper.createArrayNode();
		inputAddresses.add(address);
		input.set("addresses", inputAddresses);
		inputs.add(input);

		// outputs
		if (addressTo.toLowerCase().startsWith("0x")) {
			addressTo = addressTo.substring(2);
		}
		ArrayNode outputs = this.mapper.createArrayNode();
		ObjectNode output = this.mapper.createObjectNode();
		ArrayNode outputAddresses = this.mapper.createArrayNode();
		outputAddresses.add(addressTo);
		output.set("addresses", outputAddresses);
		output.set("value", new BigIntegerNode(weiAmount));
		outputs.add(output);

		BigInteger trxFee = fees.multiply(new BigDecimal(Denomination.ETHER.value())).toBigInteger();
		ObjectNode payload = this.mapper.createObjectNode();
		payload.set("inputs", inputs);
		payload.set("outputs", outputs);
		payload.set("fees", new BigIntegerNode(trxFee));


//		System.out.println("### skeleton : "+payload.toString());

		// get transaction skeleton
		String url = this.getUrl() + "/txs/new?token=" + this.token;
		Response response = this._post(payload, url, JSON);


		String bodyString = response.body().string();

//		System.out.println("### url : "+url);
//		System.out.println("### bodystring : "+bodyString);

//		if (!response.isSuccessful()) {
//			return this._buildResult(false, response.code(), response.message(), bodyString);
//		}

		JsonNode body = mapper.readTree(bodyString);
		if (body.has("errors")) {
			List<String> errors = new ArrayList<>();
			if (body.get("errors").isArray()) {
				for (final JsonNode node : body.get("errors")) {
					errors.add(node.toString());
				}
			} else {
				errors.add(body.get("errors").asText());
			}
			return new Result(false, errors.toArray(new String[errors.size()]), body,500);
		}

		return new Result(body);
	}

	//MOVED TO SOFT-HSM

	public Result sendTransaction(JsonNode body, List<String> signature) throws  IOException{
		// sign transaction

		ObjectNode payload = this.mapper.createObjectNode();

		ArrayNode signatures = this.mapper.createArrayNode();

		signatures = this.mapper.createArrayNode();
		for (int i = 0; i<signature.size(); i++) {
			signatures.add(signature.get(i));
		}
		payload = body.deepCopy();
		payload.set("signatures", signatures);

		String url = this.getUrl() + "/txs/send?token=" + this.token;

//		System.out.println("### url : "+url);
//		System.out.println("### payloads : "+payload.toString());

		RequestBody requestBody = RequestBody.create(TEXT, payload.toString());
		Request request = new Request.Builder()
				.url(url)
				.post(requestBody)
				.build();
		Response response = this.httpClient.newCall(request).execute();
		String bodyString = response.body().string();


//		System.out.println("### bodyString : "+bodyString);

		if (!response.isSuccessful()) {
			return new Result(false, new String[]{ String.format("%s %s: %s", response.code(), response.message(), bodyString) }, response.code());
		}

		return new Result(mapper.readTree(bodyString));
	}
}

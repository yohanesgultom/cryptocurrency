package me.gultom.cryptocurrency.ethereum;

import me.gultom.cryptocurrency.*;
import me.gultom.cryptocurrency.exception.APIErrorException;
import org.web3j.crypto.Bip39Wallet;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.Keys;
import org.web3j.crypto.MnemonicUtils;
import org.web3j.utils.Numeric;

import java.io.IOException;
import java.math.BigDecimal;

import static org.web3j.crypto.Hash.sha256;

public class EthereumHDWallet extends HDWallet {
    private Bip39Wallet wallet;
    private KeyPair currentKeyPair;
    private Network network;

    public EthereumHDWallet(Bip39Wallet wallet, Network network) {
        this.network = network;
        byte[] seed = MnemonicUtils.generateSeed(wallet.getMnemonic(), "");
        ECKeyPair keyPair = ECKeyPair.create(sha256(seed));
        String address = "0x" + Keys.getAddress(keyPair);
        String key = Numeric.toHexStringNoPrefix(keyPair.getPrivateKey());
        this.currentKeyPair = new KeyPair(address, key);
        this.wallet = wallet;
    }

    public EthereumHDWallet(Bip39Wallet wallet) {
        this(wallet, Network.MAIN);
    }

    public Network getNetwork() {
        return network;
    }

    /**
     * Get BIP-39 mnemonic code, wallet's readable seed
     * @return
     */
    public String getMnemonicCode() {
        return this.wallet.getMnemonic();
    }

    /**
     * Get wallet's current public key
     * @return
     */
    public String getPublicAddress() {
        return this.currentKeyPair.getPublicAddress();
    }

    /**
     * Get wallet's current private key
     * @return
     */
    public String getPrivateKey() {
        return this.currentKeyPair.getPrivateKey();
    }

    /**
     * Send certain amount of ETH from this wallet to address.
     *
     * @param amount ETH amount
     * @param address ETH address
     * @param config Config object containing <code>token</code>
     * @return
     * @throws IOException
     */
    public Result send(BigDecimal amount, String address, ApiConfig config) throws IOException {
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.send(amount, this.getPrivateKey(), address);
    }

    /**
     * Get current ETH balance of this wallet.
     *
     * @param config
     * @return Config object containing <code>token</code>
     * @throws IOException
     * @throws APIErrorException
     */
    public BigDecimal getBalance(ApiConfig config) throws IOException, APIErrorException {
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.getBalance(this.getPublicAddress());
    }


}

package me.gultom.cryptocurrency.ethereum;

import com.fasterxml.jackson.databind.JsonNode;
import me.gultom.cryptocurrency.*;
import me.gultom.cryptocurrency.exception.APIErrorException;
import org.ethereum.crypto.ECKey;
import org.spongycastle.util.encoders.Hex;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class EthereumWallet extends Wallet {

    public EthereumWallet(String publicAddress, String privateKey) {
        super(publicAddress, privateKey);
    }

    public EthereumWallet(String publicAddress, String privateKey, Network network) {
        super(publicAddress, privateKey, network);
        if (!publicAddress.toLowerCase().startsWith("0x")) {
            this.keyPair = new KeyPair("0x" + publicAddress, privateKey);
        }
    }

    /**
     * Send certain amount of ETH from this wallet to address.
     *
     * @param amount ETH amount
     * @param address ETH address
     * @param config Config object containing <code>token</code>
     * @return
     * @throws IOException
     */
    @Override
    public Result send(BigDecimal amount, String address, ApiConfig config) throws IOException {
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.send(amount, this.getPrivateKey(), address);
    }

    /**
     * Get current ETH balance of this wallet.
     *
     * @param config
     * @return Config object containing <code>token</code>
     * @throws IOException
     * @throws APIErrorException
     */
    @Override
    public BigDecimal getBalance(ApiConfig config) throws IOException, APIErrorException {
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.getBalance(this.getPublicAddress());
    }

    /**
     * Validate <code>publicAddress</code> and <code>privateKey</code> pair.
     *
     * @return <code>true</code> if valid and <code>false</code> if otherwise
     */
    @Override
    public boolean isValid() {
        ECKey key = ECKey.fromPrivate(Hex.decode(this.getPrivateKey()));
        return ("0x" + Hex.toHexString(key.getAddress())).equals(this.getPublicAddress());
    }

    //to etherscan
    public Result createTransactionHash(BigDecimal ethAmount, String addressTo, ApiConfig config) throws IOException{
        EtherscanClient client = new EtherscanClient(this.getNetwork(), config.getToken());
        return client.createTransactionHash(ethAmount, this.getPublicAddress(), addressTo);
    }

    //to etherscan
    public Result sendTransaction(String signature, ApiConfig config) throws IOException{
        EtherscanClient client = new EtherscanClient(this.getNetwork(), config.getToken());
        return client.sendTransaction(signature);
    }

    public BigDecimal getTransactionFee(BigDecimal ethAmount, String addressTo, ApiConfig config) throws IOException{
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.getTransactionFee(ethAmount, this.getPublicAddress(), addressTo);
    }

    //to blockcypher
    public Result createAndSendSkeletonRequest(BigDecimal ethAmount, String addressTo, ApiConfig config) throws IOException{
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.createAndSendSkeletonRequest(ethAmount, this.getPublicAddress(), addressTo);
    }

    //to blockcypher
    public Result createAndSendSkeletonRequest(BigDecimal ethAmount, BigDecimal fees, String addressTo, ApiConfig config) throws IOException{
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.createAndSendSkeletonRequest(ethAmount, fees, this.getPublicAddress(), addressTo);
    }

    //to blockcypher
    public Result sendTransaction(JsonNode body, List<String> signature, ApiConfig config) throws IOException {
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.sendTransaction(body, signature);
    }

}

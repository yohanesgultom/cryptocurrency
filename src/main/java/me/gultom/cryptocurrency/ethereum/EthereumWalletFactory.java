package me.gultom.cryptocurrency.ethereum;

import me.gultom.cryptocurrency.Network;
import me.gultom.cryptocurrency.Wallet;
import me.gultom.cryptocurrency.WalletFactory;
import org.ethereum.crypto.ECKey;
import org.spongycastle.util.encoders.Hex;

public class EthereumWalletFactory implements WalletFactory {
    @Override
    public Wallet generate() {
        ECKey key = new ECKey();
        byte[] addr = key.getAddress();
        byte[] priv = key.getPrivKeyBytes();
        return new EthereumWallet(Hex.toHexString(addr), Hex.toHexString(priv));
    }

    @Override
    public Wallet generate(Network network) {
        ECKey key = new ECKey();
        byte[] addr = key.getAddress();
        byte[] priv = key.getPrivKeyBytes();
        return new EthereumWallet(Hex.toHexString(addr), Hex.toHexString(priv), network);
    }

    @Override
    public Wallet create(String publicAddress, String privateKey) {
        return new EthereumWallet(publicAddress, privateKey);
    }

    @Override
    public Wallet create(String publicAddress, String privateKey, Network network) {
        return new EthereumWallet(publicAddress, privateKey, network);
    }
}

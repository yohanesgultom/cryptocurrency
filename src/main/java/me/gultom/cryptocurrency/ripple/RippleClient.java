package me.gultom.cryptocurrency.ripple;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ripple.core.coretypes.AccountID;
import com.ripple.core.coretypes.Amount;
import com.ripple.core.coretypes.uint.UInt32;
import com.ripple.core.types.known.tx.signed.SignedTransaction;
import com.ripple.core.types.known.tx.txns.Payment;
import com.ripple.crypto.ecdsa.IKeyPair;
import com.ripple.crypto.ecdsa.Seed;
import me.gultom.cryptocurrency.Network;
import me.gultom.cryptocurrency.Result;
import me.gultom.cryptocurrency.exception.APIErrorException;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class RippleClient {

    private final String MAIN_URL = "wss://s1.ripple.com";
    private final String TEST_URL = "wss://s.altnet.rippletest.net:51233";

    private Network network;
    private ObjectMapper mapper;

    public RippleClient(Network network) {
        this.mapper = new ObjectMapper();
        this.network = network;
    }

    private String getUrl() {
        String url;
        if (this.network.equals(Network.MAIN)) {
            url = MAIN_URL;
        } else { // TEST
            url = TEST_URL;
        }
        return url;
    }

    public BigDecimal getBalance(String address) throws APIErrorException {
        String command = "account_info";
        ObjectNode payload = this.mapper.createObjectNode();
        payload.put("command", command);
        payload.put("account", address);
        payload.put("strict", true);
        payload.put("ledger_index", "current");
        payload.put("queue", true);
        String response = null;
        BigDecimal balance = null;
        try {
            BlockingWebSocketClient client = new BlockingWebSocketClient(new URI(this.getUrl()));
            response = client.sendBlocking(payload.toString());
            JsonNode body = this.mapper.readTree(response);
            Amount amount = Amount.fromDropString(body.get("result").get("account_data").get("Balance").textValue());
            balance = amount.value();
        } catch (NullPointerException e) {
            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
        } catch (InterruptedException e) {
            throw new APIErrorException(String.format("Websocket %s error: %s", command, e.getMessage()));
        } catch (JsonProcessingException e) {
            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
        } catch (IOException e) {
            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
        } catch (URISyntaxException e) {
            throw new APIErrorException(String.format("Unexpected URL used in library: %s\n%s", this.getUrl(), e.getMessage()));
        }
        return balance;
    }


    @Deprecated
    public Result sign(BigDecimal xrpAmount, String accountId, String secret, String addressTo) throws APIErrorException {
        String command = "sign";
        ObjectNode tx = this.mapper.createObjectNode();
        Amount amount = new Amount(xrpAmount);
        tx.put("TransactionType", "Payment");
        tx.put("Account", accountId);
        tx.put("Destination", addressTo);
        tx.put("Amount", amount.toDropsString());

        ObjectNode payload = this.mapper.createObjectNode();
        payload.set("tx_json", tx);
        payload.put("command", command);
        payload.put("secret", secret);
        payload.put("offline", false);
        payload.put("fee_mult_max", 1000);

        String response = null;
        Result result;
        try {
            BlockingWebSocketClient client = new BlockingWebSocketClient(new URI(this.getUrl()));
            response = client.sendBlocking(payload.toString());
            JsonNode body = this.mapper.readTree(response);
            if (body.has("error_message")) {
                result = new Result(false, new String[]{body.get("error_message").textValue()}, 500);
            } else {
                // validate property
                body.get("result").get("tx_blob").textValue();
                result = new Result(body);
            }
        } catch (InterruptedException e) {
            throw new APIErrorException(String.format("Websocket %s error: %s", command, e.getMessage()));
        } catch (JsonProcessingException e) {
            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
        } catch (IOException e) {
            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
        } catch (NullPointerException e) {
            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
        } catch (URISyntaxException e) {
            throw new APIErrorException(String.format("Unexpected URL used in library: %s\n%s", this.getUrl(), e.getMessage()));
        }
        return result;
    }

    public JsonNode getAccountInfo(String address) throws APIErrorException {
        String command = "account_info";
        ObjectNode payload = this.mapper.createObjectNode();
        payload.put("command", command);
        payload.put("account", address);
        payload.put("strict", true);
        payload.put("ledger_index", "current");
        payload.put("queue", true);
        String response = null;
        JsonNode body = null;
        try {
            BlockingWebSocketClient client = new BlockingWebSocketClient(new URI(this.getUrl()));
            response = client.sendBlocking(payload.toString());
            body = this.mapper.readTree(response);
        } catch (NullPointerException e) {
            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
        } catch (InterruptedException e) {
            throw new APIErrorException(String.format("Websocket %s error: %s", command, e.getMessage()));
        } catch (JsonProcessingException e) {
            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
        } catch (IOException e) {
            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
        } catch (URISyntaxException e) {
            throw new APIErrorException(String.format("Unexpected URL used in library: %s\n%s", this.getUrl(), e.getMessage()));
        }
        return body;
    }

    public String signLocal(BigDecimal xrpAmount, String accountId, String secret, String addressTo, int sequence, int fee) {
        Payment payment = new Payment();
        payment.as(AccountID.Account, accountId);
        payment.as(AccountID.Destination, addressTo);
        payment.as(Amount.Amount, new Amount(xrpAmount).toDropsString());
        payment.as(UInt32.Sequence, sequence);
        payment.as(Amount.Fee, String.valueOf(fee));
        SignedTransaction signed = payment.sign(secret);
        return signed.tx_blob;
    }

    public Result send(BigDecimal xrpAmount, String privateKey, String addressTo) throws APIErrorException {
        System.out.println("### privateKey : "+privateKey);
        System.out.println("### addressTo : "+addressTo);

        Seed seed = Seed.fromBase58(privateKey);
        IKeyPair iKeyPair = seed.keyPair();
        byte[] pub160Hash = iKeyPair.pub160Hash();
        AccountID accountID = AccountID.fromBytes(pub160Hash);

////        ###
//        AccountID account = AccountID.fromAddress(addressTo);
////        BigDecimal slippageFactor = new BigDecimal("1.001");
////        BigDecimal amount = new BigDecimal("0.0000001");
//
//        Transaction transaction = new Transaction(TransactionType.Payment);
//        transaction.account(accountID);
//        transaction.;
////        ###

//        Result signResult = this.sign(xrpAmount, accountID.toString(), privateKey, addressTo);
//        if (!signResult.isSuccess()) {
//            return signResult;
//        }
//
//        System.out.println("### signResult : "+signResult.getBody());
//
//        String txBlob = signResult.getBody().get("result").get("tx_blob").textValue();

        JsonNode accountInfo = this.getAccountInfo(accountID.toString());
        int sequence = accountInfo.get("result").get("account_data").get("Sequence").intValue();
        int fee = 10000;
        String txBlob = this.signLocal(xrpAmount, accountID.toString(), privateKey, addressTo, sequence, fee);

        String command = "submit";
        ObjectNode payload = this.mapper.createObjectNode();
        payload.put("command", command);
        payload.put("tx_blob", txBlob);

        System.out.println("### payload : "+payload.toString());

        String response = null;
        Result result;
        try {
            BlockingWebSocketClient client = new BlockingWebSocketClient(new URI(this.getUrl()), 20000l);
            response = client.sendBlocking(payload.toString());
            JsonNode body = this.mapper.readTree(response);

            System.out.println("### sendBlocking : "+body.toString());

            if (body.has("error_message")) {
                result = new Result(false, new String[]{body.get("error_message").textValue()}, 500);
            } else {
                if (!"success".equalsIgnoreCase(body.get("status").textValue())
                        || body.get("result").get("engine_result_code").asInt() != 0) {
                    int code = body.get("result").get("engine_result_code").asInt();
                    result = new Result(false, new String[]{"Failed without error_message"}, body, code);
                } else {
                    result = new Result(body);
                }
            }
        } catch (InterruptedException e) {
            throw new APIErrorException(String.format("Websocket %s error: %s", command, e.getMessage()));
        } catch (JsonProcessingException e) {
            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
        } catch (IOException e) {
            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
        } catch (NullPointerException e) {
            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
        } catch (URISyntaxException e) {
            throw new APIErrorException(String.format("Unexpected URL used in library: %s\n%s", this.getUrl(), e.getMessage()));
        }
        return result;

    }

//    public Result createTransactionHash(BigDecimal xrpAmount, String address, String addressTo) throws APIErrorException {
////        Seed seed = Seed.fromBase58(privateKey);
////        IKeyPair iKeyPair = seed.keyPair();
////        byte[] pub160Hash = iKeyPair.pub160Hash();
////        AccountID accountID = AccountID.fromBytes(pub160Hash);
//
//        AccountID accountID = AccountID.fromAddress(address);
//
//        Result signResult = this.sign(xrpAmount, accountID.toString(), privateKey, addressTo);
//        if (!signResult.isSuccess()) {
//            return signResult;
//        }
//
//        String txBlob = signResult.getBody().get("result").get("tx_blob").textValue();
//        String command = "submit";
//        ObjectNode payload = this.mapper.createObjectNode();
//        payload.put("command", command);
//        payload.put("tx_blob", txBlob);
//
//        String response = null;
//        Result result;
//        try {
//            BlockingWebSocketClient client = new BlockingWebSocketClient(new URI(this.getUrl()), 20000l);
//            response = client.sendBlocking(payload.toString());
//            JsonNode body = this.mapper.readTree(response);
//            if (body.has("error_message")) {
//                result = new Result(false, new String[]{body.get("error_message").textValue()}, 500);
//            } else {
//                if (!"success".equalsIgnoreCase(body.get("status").textValue())
//                        || body.get("result").get("engine_result_code").asInt() != 0) {
//                    int code = body.get("result").get("engine_result_code").asInt();
//                    result = new Result(false, new String[]{"Failed without error_message"}, body, code);
//                } else {
//                    result = new Result(body);
//                }
//            }
//        } catch (InterruptedException e) {
//            throw new APIErrorException(String.format("Websocket %s error: %s", command, e.getMessage()));
//        } catch (JsonProcessingException e) {
//            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
//        } catch (IOException e) {
//            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
//        } catch (NullPointerException e) {
//            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
//        } catch (URISyntaxException e) {
//            throw new APIErrorException(String.format("Unexpected URL used in library: %s\n%s", this.getUrl(), e.getMessage()));
//        }
//        return result;
//    }

    public Result sendTransaction(String txBlob, BigDecimal xrpAmount, String addressTo) throws APIErrorException {
        String command = "submit";
        ObjectNode payload = this.mapper.createObjectNode();
        payload.put("command", command);
        payload.put("tx_blob", txBlob);

        String response = null;
        Result result;
        try {
            BlockingWebSocketClient client = new BlockingWebSocketClient(new URI(this.getUrl()), 20000l);
            response = client.sendBlocking(payload.toString());
            JsonNode body = this.mapper.readTree(response);
            if (body.has("error_message")) {
                result = new Result(false, new String[]{body.get("error_message").textValue()}, 500);
            } else {
                if (!"success".equalsIgnoreCase(body.get("status").textValue())
                        || body.get("result").get("engine_result_code").asInt() != 0) {
                    int code = body.get("result").get("engine_result_code").asInt();
                    result = new Result(false, new String[]{"Failed without error_message"}, body, code);
                } else {
                    result = new Result(body);
                }
            }
        } catch (InterruptedException e) {
            throw new APIErrorException(String.format("Websocket %s error: %s", command, e.getMessage()));
        } catch (JsonProcessingException e) {
            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
        } catch (IOException e) {
            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
        } catch (NullPointerException e) {
            throw new APIErrorException(String.format("Unexpected websocket %s response: %s\n%s", command, response, e.getMessage()));
        } catch (URISyntaxException e) {
            throw new APIErrorException(String.format("Unexpected URL used in library: %s\n%s", this.getUrl(), e.getMessage()));
        }
        return result;
    }

    public String signRequestOnly(BigDecimal xrpAmount, String privateKey, String addressTo) {
        Seed seed = Seed.fromBase58(privateKey);
        IKeyPair iKeyPair = seed.keyPair();
        byte[] pub160Hash = iKeyPair.pub160Hash();
        AccountID accountID = AccountID.fromBytes(pub160Hash);

        String command = "sign";
        ObjectNode tx = this.mapper.createObjectNode();
        Amount amount = new Amount(xrpAmount);
        tx.put("TransactionType", "Payment");
        tx.put("Account", accountID.toString());
        tx.put("Destination", addressTo);
        tx.put("Amount", amount.toDropsString());

        ObjectNode payload = this.mapper.createObjectNode();
        payload.set("tx_json", tx);
        payload.put("command", command);
        payload.put("secret", privateKey);
        payload.put("offline", false);
        payload.put("fee_mult_max", 1000);

        return payload.toString();
    }

    /**
     * Subscribe to account events using websocket
     * @param accountList
     * @param messageEventHandler
     * @param errorEventHandler
     * @throws URISyntaxException
     */
    public void subscribe(
            List<String> accountList,
            MessageEventHandler messageEventHandler,
            CloseEventHandler closeEventHandler,
            ErrorEventHandler errorEventHandler) throws URISyntaxException {

        ArrayNode accounts = this.mapper.createArrayNode();
        for (String account:accountList) {
            accounts.add(account);
        }

        String command = "subscribe";
        ObjectNode payload = this.mapper.createObjectNode();
        payload.put("command", command);
        payload.set("accounts", accounts);

        WebSocketClient client = new WebSocketClient(new URI(this.getUrl())) {
            @Override
            public void onOpen(ServerHandshake handshakedata) {
                this.send(payload.toString());
            }

            @Override
            public void onMessage(String message) {
                messageEventHandler.handle(message, this);
            }

            @Override
            public void onClose(int code, String reason, boolean remote) {
                closeEventHandler.handle(code, reason, remote);
            }

            @Override
            public void onError(Exception ex) {
                errorEventHandler.handle(ex, this);
            }
        };

        client.connect();
    }
}


class BlockingWebSocketClient extends WebSocketClient {

    private Long timeout;
    private List<String> results = new ArrayList<>();
    private String message;

    public BlockingWebSocketClient(URI serverUri) {
        super(serverUri);
        this.timeout = 20000l;
    }

    public BlockingWebSocketClient(URI serverUri, long timeout) {
        super(serverUri);
        this.timeout = timeout;
    }

    public String sendBlocking(String msg) throws InterruptedException {
        this.message = msg;
        this.connect();
        synchronized (this.results) {
            this.results.wait(timeout);
        }
        this.close();
        return this.results.get(0);
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        this.send(this.message);
    }

    @Override
    public void onMessage(String message) {
        synchronized (this.results) {
            this.results.add(message);
            this.results.notify();
        }
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        // do nothing
    }

    @Override
    public void onError(Exception ex) {
        ex.printStackTrace();
        this.close();
    }
}

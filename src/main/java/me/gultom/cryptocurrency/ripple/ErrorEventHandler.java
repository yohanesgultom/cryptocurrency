package me.gultom.cryptocurrency.ripple;

import org.java_websocket.client.WebSocketClient;

@FunctionalInterface
public interface ErrorEventHandler {
	void handle(Exception e, WebSocketClient client);
}

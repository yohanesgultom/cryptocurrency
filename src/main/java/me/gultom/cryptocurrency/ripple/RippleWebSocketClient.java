package me.gultom.cryptocurrency.ripple;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ripple.core.coretypes.AccountID;
import com.ripple.core.coretypes.Amount;
import com.ripple.core.coretypes.uint.UInt32;
import com.ripple.core.types.known.tx.signed.SignedTransaction;
import com.ripple.core.types.known.tx.txns.Payment;
import com.ripple.crypto.ecdsa.IKeyPair;
import com.ripple.crypto.ecdsa.Seed;
import me.gultom.cryptocurrency.Network;
import org.java_websocket.client.WebSocketClient;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

public abstract class RippleWebSocketClient extends WebSocketClient {
	protected static final String MAIN_URL = "wss://s1.ripple.com";
	protected static final String TEST_URL = "wss://s.altnet.rippletest.net:51233";

	protected Network network;
	protected ObjectMapper mapper;

	public RippleWebSocketClient(Network network) throws URISyntaxException {
		super(new URI(network.equals(Network.MAIN) ? MAIN_URL : TEST_URL));
		this.network = network;
		this.mapper = new ObjectMapper();
	}

	/**
	 * Get Account ID from secret
	 * @param secret
	 * @return Account ID
	 */
	public String getAccountID(String secret) {
		Seed seed = Seed.fromBase58(secret);
		IKeyPair iKeyPair = seed.keyPair();
		byte[] pub160Hash = iKeyPair.pub160Hash();
		AccountID accountID = AccountID.fromBytes(pub160Hash);
		return accountID.toString();
	}

	/**
	 * Generate signature locally for a payment transaction
	 * @param xrpAmount
	 * @param accountId
	 * @param secret
	 * @param addressTo
	 * @param sequence
	 * @param fee
	 * @return transaction blob
	 */
	public String sign(BigDecimal xrpAmount, String accountId, String secret, String addressTo, int sequence, int fee) {
		Payment payment = new Payment();
		payment.as(AccountID.Account, accountId);
		payment.as(AccountID.Destination, addressTo);
		payment.as(Amount.Amount, new Amount(xrpAmount).toDropsString());
		payment.as(UInt32.Sequence, sequence);
		payment.as(Amount.Fee, String.valueOf(fee));
		SignedTransaction signed = payment.sign(secret);
		return signed.tx_blob;
	}

	/**
	 * Returns String payload for account_info API
	 * @param address
	 * @return payload for account_info API
	 */
	public String getAccountInfoPayload(String address) {
		String command = "account_info";
		ObjectNode payload = this.mapper.createObjectNode();
		payload.put("command", command);
		payload.put("account", address);
		payload.put("strict", true);
		payload.put("ledger_index", "current");
		payload.put("queue", true);
		return payload.toString();
	}

	/**
	 * Returns BigDecimal balance from account_info API response
	 * @param body JsonNode object of API response body
	 * @return balance
	 * @throws IOException
	 */
	public BigDecimal getBalanceFromAccountInfoResponse(JsonNode body) {
		Amount amount = Amount.fromDropString(body.get("result").get("account_data").get("Balance").textValue());
		return amount.value();
	}

	/**
	 * Returns Integer sequence from account_info API response
	 * @param body JsonNode object of API response body
	 * @return sequence
	 * @throws IOException
	 */
	public Integer getSequenceFromAccountInfoResponse(JsonNode body) {
		int sequence = body.get("result").get("account_data").get("Sequence").intValue();
		return sequence;
	}

	/**
	 * Get String payload for submit API
	 * @param txBlob signed transaction blob
	 * @return payload
	 */
	public String getSubmitTransactionPayload(String txBlob) {
		String command = "submit";
		ObjectNode payload = this.mapper.createObjectNode();
		payload.put("command", command);
		payload.put("tx_blob", txBlob);
		return payload.toString();
	}

	/**
	 * Get String payload for subscribe accounts API
	 * @param accountList list of account IDs
	 * @return payload
	 */
	public String getSubscribeAccountsPayload(List<String> accountList) {
		ArrayNode accounts = this.mapper.createArrayNode();
		for (String account:accountList) {
			accounts.add(account);
		}
		String command = "subscribe";
		ObjectNode payload = this.mapper.createObjectNode();
		payload.put("command", command);
		payload.set("accounts", accounts);
		return payload.toString();
	}

}

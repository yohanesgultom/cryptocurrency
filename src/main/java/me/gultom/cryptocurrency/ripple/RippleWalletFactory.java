package me.gultom.cryptocurrency.ripple;

import com.ripple.config.Config;
import com.ripple.core.coretypes.AccountID;
import com.ripple.crypto.ecdsa.IKeyPair;
import com.ripple.crypto.ecdsa.Seed;
import me.gultom.cryptocurrency.Network;
import me.gultom.cryptocurrency.Wallet;
import me.gultom.cryptocurrency.WalletFactory;

import java.security.SecureRandom;

public class RippleWalletFactory implements WalletFactory {
    @Override
    public Wallet generate() {
        Config.initBouncy();
        SecureRandom random = new SecureRandom();
        byte[] seedBytes = new byte[16];
        random.nextBytes(seedBytes);
        Seed seed = new Seed(seedBytes);
        IKeyPair iKeyPair = seed.keyPair();
        byte[] pub160Hash = iKeyPair.pub160Hash();
        AccountID accountID = AccountID.fromBytes(pub160Hash);
        return new RippleWallet(accountID.toString(), seed.toString());
    }

    @Override
    public Wallet generate(Network network) {
        return this.generate();
    }

    @Override
    public Wallet create(String publicAddress, String privateKey) {
        return new RippleWallet(publicAddress, privateKey);
    }

    @Override
    public Wallet create(String publicAddress, String privateKey, Network network) {
        return new RippleWallet(publicAddress, privateKey, network);
    }
}

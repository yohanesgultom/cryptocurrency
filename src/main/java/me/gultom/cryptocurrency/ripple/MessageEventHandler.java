package me.gultom.cryptocurrency.ripple;

import org.java_websocket.client.WebSocketClient;

@FunctionalInterface
public interface MessageEventHandler {
	void handle(String message, WebSocketClient client);
}

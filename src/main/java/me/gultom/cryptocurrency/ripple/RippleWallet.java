package me.gultom.cryptocurrency.ripple;

import com.ripple.core.coretypes.AccountID;
import com.ripple.crypto.ecdsa.IKeyPair;
import com.ripple.crypto.ecdsa.Seed;
import me.gultom.cryptocurrency.ApiConfig;
import me.gultom.cryptocurrency.Network;
import me.gultom.cryptocurrency.Result;
import me.gultom.cryptocurrency.Wallet;
import me.gultom.cryptocurrency.exception.APIErrorException;

import java.math.BigDecimal;

public class RippleWallet extends Wallet {

    public RippleWallet(String publicAddress, String privateKey) {
        super(publicAddress, privateKey);
    }

    public RippleWallet(String publicAddress, String privateKey, Network network) {
        super(publicAddress, privateKey, network);
    }

    @Override
    public Result send(BigDecimal amount, String address, ApiConfig config) throws APIErrorException {
        RippleClient client = new RippleClient(this.getNetwork());
//        return client.send(amount, this.getPrivateKey(), address);
        return client.send(amount, "sscGc1S9kpfo3StA7AzQDpNrUZrGU", "rJQjJCDaNRKS8gMES8qcHT1kWWQfrjVdCN");
    }

    @Override
    public BigDecimal getBalance(ApiConfig config) throws APIErrorException {
        RippleClient client = new RippleClient(this.getNetwork());
        return client.getBalance(this.getPublicAddress());
    }

    @Override
    public boolean isValid() {
        Seed seed = Seed.fromBase58(this.getPrivateKey());
        IKeyPair iKeyPair = seed.keyPair();
        byte[] pub160Hash = iKeyPair.pub160Hash();
        AccountID accountID = AccountID.fromBytes(pub160Hash);
        return accountID.toString().equals(this.getPublicAddress());
    }
}

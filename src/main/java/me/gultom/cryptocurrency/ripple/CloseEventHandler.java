package me.gultom.cryptocurrency.ripple;

@FunctionalInterface
public interface CloseEventHandler {
	void handle(int code, String reason, boolean remote);
}

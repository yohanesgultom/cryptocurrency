package me.gultom.cryptocurrency.exception;

public class APIErrorException extends Exception {

    public APIErrorException(String message) {
        super(message);
    }
}

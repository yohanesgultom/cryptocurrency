package me.gultom.cryptocurrency;

import com.fasterxml.jackson.databind.JsonNode;

public class Result {
    private boolean success;
    private String[] errors;
    private JsonNode body;
    private int code;

    public Result() {
        this.success = true;
        this.code = 200;
    }

    public Result(boolean success, String[] errors, JsonNode body, int code) {
        this.success = success;
        this.errors = errors;
        this.body = body;
        this.code = code;
    }

    public Result(boolean success, String[] errors, int code) {
        this.success = success;
        this.errors = errors;
        this.code = code;
    }

    public Result(JsonNode body) {
        this.success = true;
        this.body = body;
        this.code = 200;
    }

    public boolean isSuccess() {
        return success;
    }

    public String[] getErrors() {
        return errors;
    }

    public JsonNode getBody() {
        return body;
    }

    public int getCode() {
        return code;
    }
}

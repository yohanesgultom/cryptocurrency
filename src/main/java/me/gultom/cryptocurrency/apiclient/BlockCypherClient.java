package me.gultom.cryptocurrency.apiclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.squareup.okhttp.*;
import me.gultom.cryptocurrency.Network;
import me.gultom.cryptocurrency.Result;
import me.gultom.cryptocurrency.exception.APIErrorException;

import java.io.IOException;
import java.math.BigDecimal;

abstract public class BlockCypherClient {
	protected final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
	protected final MediaType TEXT = MediaType.parse("text/plain; charset=utf-8");

	protected Network network;
	protected String token;
	protected OkHttpClient httpClient;
	protected ObjectMapper mapper;

	protected BlockCypherClient(Network network, String token) {
		this.network = network;
		this.token = token;
		this.httpClient = new OkHttpClient();
		this.mapper = new ObjectMapper();
	}

	protected Result _buildResult(Response response) throws IOException {
		String bodyString = response.body().string();
		return this._buildResult(response.isSuccessful(), response.code(), response.message(), bodyString);
	}

	protected Result _buildResult(boolean success, int code, String message, String bodyString) throws IOException {
		if (!success) {
			return new Result(false, new String[]{ String.format("%s %s: %s", code, message, bodyString) }, code);
		}
		return new Result(mapper.readTree(bodyString));
	}


	protected Response _post(ObjectNode payload, String url, MediaType mediaType) throws IOException {
		RequestBody requestBody = RequestBody.create(mediaType, payload.toString());
		Request request = new Request.Builder()
				.url(url)
				.post(requestBody)
				.build();
		return this.httpClient.newCall(request).execute();
	}

	/**
	 * Register/create a webhook
	 * @param eventType
	 * @param address
	 * @param callbackURL
	 * @return Result Contains BlockCypher Event on success (store the id for checking/deleting later)
	 * @throws IOException
	 */
	public Result registerWebhook(String eventType, String address, String callbackURL) throws IOException {
		String url = this.getUrl() + "/hooks?token=" + this.token;
		ObjectNode payload = this.mapper.createObjectNode();
		payload.put("event", eventType);
		payload.put("address", address);
		payload.put("url", callbackURL);
		Response response = this._post(payload, url, JSON);
		return this._buildResult(response);
	}

	/**
	 * List all webhooks registered under given token
	 * @return Result Contains array of BlockCypher Events on success
	 * @throws IOException
	 */
	public Result listWebhooks() throws IOException {
		String url = this.getUrl() + "/hooks?token=" + this.token;
		Request request = new Request.Builder().url(url).get().build();
		Response response = this.httpClient.newCall(request).execute();
		return this._buildResult(response);
	}

	/**
	 * Get a webhook by id (returned during registration/creation)
	 * @return Result Contains BlockCypher Event on success
	 * @throws IOException
	 */
	public Result getWebhook(String id) throws IOException {
		String url = this.getUrl() + "/hooks/" + id + "?token=" + this.token;
		Request request = new Request.Builder().url(url).get().build();
		Response response = this.httpClient.newCall(request).execute();
		return this._buildResult(response);
	}

	/**
	 * Get a webhook by id (returned during registration/creation)
	 * @return Result Empty (no content) on success
	 * @throws IOException
	 */
	public Result deleteWebhook(String id) throws IOException {
		String url = this.getUrl() + "/hooks/" + id + "?token=" + this.token;
		Request request = new Request.Builder().url(url).delete().build();
		Response response = this.httpClient.newCall(request).execute();
		if (response.isSuccessful()) {
			return new Result(); // empty on success
		} else {
			String bodyString = response.body().string();
			return this._buildResult(response.isSuccessful(), response.code(), response.message(), bodyString);
		}
	}

	abstract protected String getUrl();
	abstract public BigDecimal getBalance(String address) throws IOException, APIErrorException;
	abstract public Result send(BigDecimal ethAmount, String privateKey, String addressTo) throws IOException;
	abstract public BigDecimal getReceivedAmountFromTX(String json, String address) throws IOException;

}

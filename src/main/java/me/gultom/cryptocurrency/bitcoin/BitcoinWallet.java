package me.gultom.cryptocurrency.bitcoin;

import com.fasterxml.jackson.databind.JsonNode;
import me.gultom.cryptocurrency.ApiConfig;
import me.gultom.cryptocurrency.Network;
import me.gultom.cryptocurrency.Result;
import me.gultom.cryptocurrency.Wallet;
import me.gultom.cryptocurrency.exception.APIErrorException;
import org.bitcoinj.core.DumpedPrivateKey;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.TestNet3Params;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class BitcoinWallet extends Wallet {

    public BitcoinWallet(String publicAddress, String privateKey) {
        super(publicAddress, privateKey);
    }

    public BitcoinWallet(String publicAddress, String privateKey, Network network) {
        super(publicAddress, privateKey, network);
    }

    @Override
    public Result send(BigDecimal amount, String address, ApiConfig config) throws IOException {
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.send(amount, this.getPrivateKey(), address);
    }

    @Override
    public BigDecimal getBalance(ApiConfig config) throws IOException, APIErrorException {
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.getBalance(this.getPublicAddress());
    }

    /**
     * Validate public address - private key pair in given network
      * @return
     */
    @Override
    public boolean isValid() {
        String wif = this.getPrivateKey();
        DumpedPrivateKey dpk = DumpedPrivateKey.fromBase58(null, wif);
        ECKey key = dpk.getKey();
        return key
                .toAddress(getNetworkParameter(this.getNetwork()))
                .toString()
                .equals(this.getPublicAddress());
    }

    public static NetworkParameters getNetworkParameter(Network network) {
        if (network.equals(Network.MAIN)) {
            return MainNetParams.get();
        } else { // network.equals(Network.TEST)
            return TestNet3Params.get();
        }
    }


    public String createSkeletonRequest(BigDecimal btcAmount, String addressTo, ApiConfig config){
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.createSkeletonRequest(btcAmount, this.getPublicAddress(), addressTo);
    }

    public Result sendSkeletonRequest(String wrappedSkeletonRequest, ApiConfig config) throws IOException{
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.sendSkeletonRequest(wrappedSkeletonRequest);
    }

    public BigDecimal getTransactionFee(BigDecimal btcAmount, String addressTo, ApiConfig config) throws IOException{
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.getTransactionFee(btcAmount, this.getPublicAddress(), addressTo);
    }

    public Result createAndSendSkeletonRequest(BigDecimal btcAmount, String addressTo, ApiConfig config) throws IOException{
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.createAndSendSkeletonRequest(btcAmount, this.getPublicAddress(), addressTo);
    }

    public Result createAndSendSkeletonRequest(BigDecimal btcAmount, BigDecimal fees, String addressTo, ApiConfig config) throws IOException{
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.createAndSendSkeletonRequest(btcAmount, fees, this.getPublicAddress(), addressTo);
    }

//    public String signTransaction(String privateKey, String wrappedSkeletonResponse, ApiConfig config) throws IOException{
//        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
//        return client.signTransaction(privateKey, wrappedSkeletonResponse);
//    }

    public Result sendTransaction(JsonNode body, List<String> publicKey, List<String> signature, ApiConfig config) throws IOException {
        BlockCypherClient client = new BlockCypherClient(this.getNetwork(), config.getToken());
        return client.sendTransaction(body, publicKey, signature);
    }

}

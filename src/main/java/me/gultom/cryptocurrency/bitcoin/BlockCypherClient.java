package me.gultom.cryptocurrency.bitcoin;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.squareup.okhttp.*;
import me.gultom.cryptocurrency.Network;
import me.gultom.cryptocurrency.Result;
import me.gultom.cryptocurrency.exception.APIErrorException;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.DumpedPrivateKey;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.Sha256Hash;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * This is the client for some APIs in https://www.blockcypher.com/dev/bitcoin
 *
 */
public class BlockCypherClient extends me.gultom.cryptocurrency.apiclient.BlockCypherClient {

    // Supported event types https://www.blockcypher.com/dev/bitcoin/#types-of-events
    public interface Event {
        // callback payload is tx https://www.blockcypher.com/dev/bitcoin/#tx
        String UNCONFIRMEDTX = "unconfirmed-tx";
        String CONFIRMEDTX = "confirmed-tx";
    }

    private final String MAIN_URL = "https://api.blockcypher.com/v1/btc/main";
    private final String TESTNET_URL = "https://api.blockcypher.com/v1/btc/test3";

    public BlockCypherClient(Network network, String token) {
        super(network, token);
    }

    protected String getUrl() {
        String url;
        if (this.network.equals(Network.MAIN)) {
            url = MAIN_URL;
        } else { // TEST
            url = TESTNET_URL;
        }
        return url;
    }

    /**
     * Get (confirmed) balance in Satoshi and return as BTC
     * @param address
     * @return BigDecimal BTC amount
     * @throws IOException
     * @throws APIErrorException
     */
    public BigDecimal getBalance(String address) throws IOException, APIErrorException {
        String url = String.format("%s/addrs/%s/balance", this.getUrl(), address);
        Request request = new Request.Builder().url(url).get().build();
        Response response = this.httpClient.newCall(request).execute();
        String bodyString = response.body().string();
        if (!response.isSuccessful()) {
            throw new APIErrorException(String.format("%s %s", response.code(), response.message()));
        } else {
            JsonNode body = mapper.readTree(bodyString);
            if (!body.has("balance")) {
                throw new APIErrorException(String.format("Unexpected response: %s", bodyString));
            } else {
                Long satoshi = body.get("balance").asLong();
                return new BigDecimal(Coin.valueOf(satoshi).toPlainString());
            }
        }
    }

    /**
     * Send BTC amount using two-endpoint transaction creation tool
     * https://www.blockcypher.com/dev/bitcoin/#creating-transactions
     * @param btcAmount
     * @param privateKey
     * @param addressTo
     * @return Result API result
     * @throws IOException
     */
    public Result send(BigDecimal btcAmount, String privateKey, String addressTo) throws IOException {

        DumpedPrivateKey dpk = DumpedPrivateKey.fromBase58(null, privateKey);
        ECKey key = dpk.getKey();
        String addressFrom = key.toAddress(BitcoinWallet.getNetworkParameter(this.network)).toString();

        // inputs
        ArrayNode inputs = this.mapper.createArrayNode();
        ObjectNode input = this.mapper.createObjectNode();
        ArrayNode inputAddresses = this.mapper.createArrayNode();
        inputAddresses.add(addressFrom);
        input.set("addresses", inputAddresses);
        inputs.add(input);

        // outputs
        Coin amount = Coin.valueOf(btcAmount.multiply(new BigDecimal(Coin.COIN.value)).toBigInteger().longValue());

        ArrayNode outputs = this.mapper.createArrayNode();
        ObjectNode output = this.mapper.createObjectNode();
        ArrayNode outputAddresses = this.mapper.createArrayNode();
        outputAddresses.add(addressTo);
        output.set("addresses", outputAddresses);
        output.put("value", amount.getValue());
        outputs.add(output);

        ObjectNode payload = this.mapper.createObjectNode();
        payload.set("inputs", inputs);
        payload.set("outputs", outputs);

        // get transaction skeleton
        String url = this.getUrl() + "/txs/new";
        RequestBody requestBody = RequestBody.create(JSON, payload.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Response response = this.httpClient.newCall(request).execute();
        String bodyString = response.body().string();

        if (!response.isSuccessful()) {
            return new Result(false, new String[]{ String.format("%s %s: %s", response.code(), response.message(), bodyString) }, response.code());
        }

        JsonNode body = mapper.readTree(bodyString);
        if (body.has("errors")) {
            List<String> errors = new ArrayList<>();
            if (body.get("errors").isArray()) {
                for (final JsonNode node : body.get("errors")) {
                    errors.add(node.textValue());
                }
            } else {
                errors.add(body.get("errors").textValue());
            }
            return new Result(false, errors.toArray(new String[0]), 500);
        }

        // sign transaction
        ArrayNode pubkeys = this.mapper.createArrayNode();
        ArrayNode signatures = this.mapper.createArrayNode();
        for (JsonNode node : body.get("tosign")) {
            String msg = node.asText().trim();
            Sha256Hash hash = Sha256Hash.wrap(msg);
            ECKey.ECDSASignature sig = key.sign(hash);
            byte[] sigByte = sig.encodeToDER();
            String signed = DatatypeConverter.printHexBinary(sigByte);
            signatures.add(signed);
            pubkeys.add(key.getPublicKeyAsHex());
        }

        payload = body.deepCopy();
        payload.set("pubkeys", pubkeys);
        payload.set("signatures", signatures);

        // submit signed transaction
        url = this.getUrl() + "/txs/send?token=" + this.token;
        requestBody = RequestBody.create(TEXT, payload.toString());
        request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        response = this.httpClient.newCall(request).execute();
        bodyString = response.body().string();

        if (!response.isSuccessful()) {
            return new Result(false, new String[]{ String.format("%s %s: %s", response.code(), response.message(), bodyString) }, response.code());
        }

        return new Result(mapper.readTree(bodyString));
    }

    /**
     * Example of how to extract received amount from webhook TX payload
     * @param json TX JSON string https://www.blockcypher.com/dev/bitcoin/#tx
     * @param address Receiving (output) address
     * @return BigDecimal Received amount in BTC
     * @throws IOException
     */
    public BigDecimal getReceivedAmountFromTX(String json, String address) throws IOException {
        BigDecimal amount = null;
        JsonNode tx = mapper.readTree(json);
        if (!tx.hasNonNull("outputs") || !tx.get("outputs").isArray()) {
            throw new IOException("No outputs field found");
        }
        boolean found = false;
        for (JsonNode output:tx.get("outputs")) {
            if (output.hasNonNull("addresses") && output.get("addresses").isArray()) {
                for (JsonNode outAddress:output.get("addresses")) {
                    if (address.equalsIgnoreCase(outAddress.asText())) {
                        if (!output.hasNonNull("value")) {
                            throw new IOException("Output address found but no value field found");
                        }
                        Long satoshi = output.get("value").asLong();
                        amount = new BigDecimal(Coin.valueOf(satoshi).toPlainString());
                        found = true;
                        break;
                    }
                }
            }
            if (found) break;
        }
        return amount;
    }

    public String createSkeletonRequest( BigDecimal btcAmount, String address, String addressTo){

        // inputs
        ArrayNode inputs = this.mapper.createArrayNode();
        ObjectNode input = this.mapper.createObjectNode();
        ArrayNode inputAddresses = this.mapper.createArrayNode();
        inputAddresses.add(address);
        input.set("addresses", inputAddresses);
        inputs.add(input);

        // outputs
        Coin amount = Coin.valueOf(btcAmount.multiply(new BigDecimal(Coin.COIN.value)).toBigInteger().longValue());

        ArrayNode outputs = this.mapper.createArrayNode();
        ObjectNode output = this.mapper.createObjectNode();
        ArrayNode outputAddresses = this.mapper.createArrayNode();
        outputAddresses.add(addressTo);
        output.set("addresses", outputAddresses);
        output.put("value", amount.getValue());
        outputs.add(output);

        ObjectNode payload = this.mapper.createObjectNode();
        payload.set("inputs", inputs);
        payload.set("outputs", outputs);

        return payload.toString();
    }

    public Result sendSkeletonRequest(String wrappedSkeletonRequest) throws IOException{
        // get transaction skeleton
        String url = this.getUrl() + "/txs/new";
        RequestBody requestBody = RequestBody.create(JSON, wrappedSkeletonRequest);
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Response response = this.httpClient.newCall(request).execute();
        String bodyString = response.body().string();

        if (!response.isSuccessful()) {
            return new Result(false, new String[]{ String.format("%s %s: %s", response.code(), response.message(), bodyString) }, response.code());
        }


        JsonNode body = mapper.readTree(bodyString);
        if (body.has("errors")) {
            List<String> errors = new ArrayList<>();
            if (body.get("errors").isArray()) {
                for (final JsonNode node : body.get("errors")) {
                    errors.add(node.textValue());
                }
            } else {
                errors.add(body.get("errors").textValue());
            }
            return new Result(false, errors.toArray(new String[0]), 500);
        }

        return new Result(mapper.readTree(bodyString));

    }

    public BigDecimal getTransactionFee( BigDecimal btcAmount, String address, String addressTo) throws  IOException{

        // inputs
        ArrayNode inputs = this.mapper.createArrayNode();
        ObjectNode input = this.mapper.createObjectNode();
        ArrayNode inputAddresses = this.mapper.createArrayNode();
        inputAddresses.add(address);
        input.set("addresses", inputAddresses);
        inputs.add(input);

        // outputs
        Coin amount = Coin.valueOf(btcAmount.multiply(new BigDecimal(Coin.COIN.value)).toBigInteger().longValue());

        ArrayNode outputs = this.mapper.createArrayNode();
        ObjectNode output = this.mapper.createObjectNode();
        ArrayNode outputAddresses = this.mapper.createArrayNode();
        outputAddresses.add(addressTo);
        output.set("addresses", outputAddresses);
        output.put("value", amount.getValue());
        outputs.add(output);

        ObjectNode payload = this.mapper.createObjectNode();
        payload.set("inputs", inputs);
        payload.set("outputs", outputs);

        // get transaction skeleton
        String url = this.getUrl() + "/txs/new";

//        System.out.println("#### url : "+url);
//        System.out.println("#### skeleton : "+payload.toString());

        RequestBody requestBody = RequestBody.create(JSON, payload.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Response response = this.httpClient.newCall(request).execute();
        String bodyString = response.body().string();

//        System.out.println("#### bodystring : "+bodyString);

        JsonNode body = mapper.readTree(bodyString);
        JsonNode txBody = body.get("tx");

        Long inputSize = txBody.get("vin_sz").asLong();
        Long outputSize = txBody.get("vout_sz").asLong();
        Long totalBytes = (148 * inputSize) + (34 * outputSize) + 10;
        Long feePerByte = 26L; //see bitcoinfees.earn.com

        return new BigDecimal(Coin.valueOf(totalBytes * feePerByte).toPlainString());
    }

    public Result createAndSendSkeletonRequest( BigDecimal btcAmount, String address, String addressTo) throws  IOException{

        // inputs
        ArrayNode inputs = this.mapper.createArrayNode();
        ObjectNode input = this.mapper.createObjectNode();
        ArrayNode inputAddresses = this.mapper.createArrayNode();
        inputAddresses.add(address);
        input.set("addresses", inputAddresses);
        inputs.add(input);

        // outputs
        Coin amount = Coin.valueOf(btcAmount.multiply(new BigDecimal(Coin.COIN.value)).toBigInteger().longValue());

        ArrayNode outputs = this.mapper.createArrayNode();
        ObjectNode output = this.mapper.createObjectNode();
        ArrayNode outputAddresses = this.mapper.createArrayNode();
        outputAddresses.add(addressTo);
        output.set("addresses", outputAddresses);
        output.put("value", amount.getValue());
        outputs.add(output);

        ObjectNode payload = this.mapper.createObjectNode();
        payload.set("inputs", inputs);
        payload.set("outputs", outputs);

        // get transaction skeleton
        String url = this.getUrl() + "/txs/new";

//        System.out.println("#### url : "+url);
//        System.out.println("#### skeleton : "+payload.toString());

        RequestBody requestBody = RequestBody.create(JSON, payload.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Response response = this.httpClient.newCall(request).execute();
        String bodyString = response.body().string();

//        System.out.println("#### bodyString : "+bodyString);

        JsonNode body = mapper.readTree(bodyString);

//        if (!response.isSuccessful()) {
//            return new Result(false, new String[]{ String.format("%s %s: %s", response.code(), response.message(), bodyString) }, body, response.code());
//        }

        if (body.has("errors")) {
            List<String> errors = new ArrayList<>();
            if (body.get("errors").isArray()) {
                for (final JsonNode node : body.get("errors")) {
                    errors.add(node.toString());
                }
            } else {
                errors.add(body.get("errors").asText());
            }
            return new Result(false, errors.toArray(new String[errors.size()]), body, 500);
        }

//        System.out.println("### resultCode "+result.getCode());
//        System.out.println("### resulcBOdy "+result.getBody());

        return new Result(body);
    }

    public Result createAndSendSkeletonRequest( BigDecimal btcAmount, BigDecimal fee,  String address, String addressTo) throws  IOException{

        // inputs
        ArrayNode inputs = this.mapper.createArrayNode();
        ObjectNode input = this.mapper.createObjectNode();
        ArrayNode inputAddresses = this.mapper.createArrayNode();
        inputAddresses.add(address);
        input.set("addresses", inputAddresses);
        inputs.add(input);

        // outputs
        Coin amount = Coin.valueOf(btcAmount.multiply(new BigDecimal(Coin.COIN.value)).toBigInteger().longValue());

        ArrayNode outputs = this.mapper.createArrayNode();
        ObjectNode output = this.mapper.createObjectNode();
        ArrayNode outputAddresses = this.mapper.createArrayNode();
        outputAddresses.add(addressTo);
        output.set("addresses", outputAddresses);
        output.put("value", amount.getValue());
        outputs.add(output);

        //fees
        Coin trxFee = Coin.valueOf(fee.multiply(new BigDecimal(Coin.COIN.value)).toBigInteger().longValue());
        ObjectNode payload = this.mapper.createObjectNode();
        payload.set("inputs", inputs);
        payload.set("outputs", outputs);
        payload.put("fees", trxFee.getValue());

        // get transaction skeleton
        String url = this.getUrl() + "/txs/new";

//        System.out.println("#### url : "+url);
//        System.out.println("#### skeleton : "+payload.toString());

        RequestBody requestBody = RequestBody.create(JSON, payload.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Response response = this.httpClient.newCall(request).execute();
        String bodyString = response.body().string();

//        System.out.println("#### bodyString : "+bodyString);

//        if (!response.isSuccessful()) {
//            return new Result(false, new String[]{ String.format("%s %s: %s", response.code(), response.message(), bodyString) }, response.code());
//        }

        JsonNode body = mapper.readTree(bodyString);

        if (body.has("errors")) {
            List<String> errors = new ArrayList<>();
            if (body.get("errors").isArray()) {
                for (final JsonNode node : body.get("errors")) {
                    errors.add(node.toString());
                }
            } else {
                errors.add(body.get("errors").asText());
            }
            return new Result(false, errors.toArray(new String[errors.size()]), body, 500);
        }

        return new Result(body);
    }

    //MOVED TO SOFT-HSM
//    public String signTransaction(String privateKey, String wrappedSkeletonResponse) throws IOException {
//        JsonNode body = mapper.readTree(wrappedSkeletonResponse);
//        DumpedPrivateKey dpk = DumpedPrivateKey.fromBase58(null, privateKey);
//        ECKey key = dpk.getKey();
//
//        // sign transaction
//        ArrayNode pubkeys = this.mapper.createArrayNode();
//        ArrayNode signatures = this.mapper.createArrayNode();
//        for (JsonNode node : body.get("tosign")) {
//            String msg = node.asText().trim();
//            Sha256Hash hash = Sha256Hash.wrap(msg);
//            ECKey.ECDSASignature sig = key.sign(hash);
//            byte[] sigByte = sig.encodeToDER();
//            String signed = DatatypeConverter.printHexBinary(sigByte);
//            signatures.add(signed);
//            pubkeys.add(key.getPublicKeyAsHex());
//        }
//
//        ObjectNode payload = this.mapper.createObjectNode();
//
//        payload = body.deepCopy();
//        payload.set("pubkeys", pubkeys);
//        payload.set("signatures", signatures);
//
//        return payload.toString();
//
//    }

    public Result sendTransaction(JsonNode body, List<String> publicKey, List<String> signature) throws  IOException{
        // submit signed transaction
        ObjectNode payload = this.mapper.createObjectNode();

        ArrayNode pubkeys = this.mapper.createArrayNode();
        ArrayNode signatures = this.mapper.createArrayNode();
        for (int i = 0; i<publicKey.size(); i++) {
            pubkeys.add(publicKey.get(i));
            signatures.add(signature.get(i));
        }

        payload = body.deepCopy();
        payload.set("pubkeys", pubkeys);
        payload.set("signatures", signatures);

        String url = this.getUrl() + "/txs/send?token=" + this.token;

//        System.out.println("### url : "+url);
//        System.out.println("### payloads : "+payload.toString());

        RequestBody requestBody = RequestBody.create(TEXT, payload.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Response response = this.httpClient.newCall(request).execute();
        String bodyString = response.body().string();


//        System.out.println("### bodyString : "+bodyString);

        if (!response.isSuccessful()) {
            return new Result(false, new String[]{ String.format("%s %s: %s", response.code(), response.message(), bodyString) }, response.code());
        }

        return new Result(mapper.readTree(bodyString));
    }
}

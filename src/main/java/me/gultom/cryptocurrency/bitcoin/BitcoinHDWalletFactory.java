package me.gultom.cryptocurrency.bitcoin;

import me.gultom.cryptocurrency.HDWallet;
import me.gultom.cryptocurrency.Network;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.wallet.DeterministicSeed;

public class BitcoinHDWalletFactory {

    public HDWallet generate() {
        return this.generate(Network.MAIN);
    }

    public HDWallet generate(Network network) {
        NetworkParameters netParam = BitcoinWallet.getNetworkParameter(network);
        org.bitcoinj.wallet.Wallet wallet = new org.bitcoinj.wallet.Wallet(netParam);
        return new BitcoinHDWallet(wallet);
    }

    public HDWallet create(String mnemonicCode, long creationTimeSeconds) throws Exception {
        return this.create(mnemonicCode, creationTimeSeconds, 0, Network.MAIN);
    }

    public HDWallet create(String mnemonicCode, long creationTimeSeconds, int counter, Network network) throws Exception {
        DeterministicSeed seed = new DeterministicSeed(mnemonicCode, null, "", creationTimeSeconds);
        NetworkParameters netParam = BitcoinWallet.getNetworkParameter(network);
        org.bitcoinj.wallet.Wallet wallet = org.bitcoinj.wallet.Wallet.fromSeed(netParam, seed);
        return new BitcoinHDWallet(wallet, counter);
    }

}

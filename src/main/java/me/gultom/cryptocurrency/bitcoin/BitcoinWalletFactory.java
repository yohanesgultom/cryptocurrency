package me.gultom.cryptocurrency.bitcoin;

import me.gultom.cryptocurrency.Network;
import me.gultom.cryptocurrency.Wallet;
import me.gultom.cryptocurrency.WalletFactory;
import org.bitcoinj.core.Address;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.params.MainNetParams;

public class BitcoinWalletFactory implements WalletFactory {

    @Override
    public Wallet generate() {
        NetworkParameters netParam = MainNetParams.get();
        ECKey key = new ECKey();
        Address address = key.toAddress(netParam);
        return new BitcoinWallet(
                address.toString(),
                key.getPrivateKeyAsWiF(netParam));
    }

    @Override
    public Wallet generate(Network network) {
        NetworkParameters netParam = BitcoinWallet.getNetworkParameter(network);
        ECKey key = new ECKey();
        Address address = key.toAddress(netParam);
        return new BitcoinWallet(
                address.toString(),
                key.getPrivateKeyAsWiF(netParam),
                network
        );
    }

    @Override
    public Wallet create(String publicAddress, String privateKey) {
        return new BitcoinWallet(
                publicAddress,
                privateKey,
                Network.MAIN
        );
    }

    @Override
    public Wallet create(String publicAddress, String privateKey, Network network) {
        return new BitcoinWallet(
                publicAddress,
                privateKey,
                network
        );
    }

}

package me.gultom.cryptocurrency;

public class KeyPair {
    String publicAddress;
    String privateKey;

    public KeyPair(String publicAddress, String privateKey) {
        this.publicAddress = publicAddress;
        this.privateKey = privateKey;
    }

    public String getPublicAddress() {
        return publicAddress;
    }

    public String getPrivateKey() {
        return privateKey;
    }
}

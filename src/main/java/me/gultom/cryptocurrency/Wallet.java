package me.gultom.cryptocurrency;

import me.gultom.cryptocurrency.exception.APIErrorException;

import java.io.IOException;
import java.math.BigDecimal;

public abstract class Wallet {
    protected KeyPair keyPair;
    private Network network;

    public Wallet(String publicAddress, String privateKey) {
        this.keyPair = new KeyPair(publicAddress, privateKey);
        this.network = Network.MAIN;
    }

    public Wallet(String publicAddress, String privateKey, Network network) {
        this.keyPair = new KeyPair(publicAddress, privateKey);
        this.network = network;
    }

    public String getPublicAddress() {
        return this.keyPair.getPublicAddress();
    }

    public String getPrivateKey() {
        return this.keyPair.getPrivateKey();
    }

    public Network getNetwork() {
        return this.network;
    }

    public abstract Result send(BigDecimal amount, String address, ApiConfig config) throws IOException, APIErrorException;
    public abstract BigDecimal getBalance(ApiConfig config) throws IOException, APIErrorException;
    public abstract boolean isValid();

    @Override
    public String toString() {
        return String.format(
                "Class: %s\nNetwork: %s\nPublic Address: %s\nPrivate Key: %s\n",
                this.getClass().toString(),
                this.getNetwork().toString(),
                this.keyPair.getPublicAddress(),
                this.keyPair.getPrivateKey()
        );
    }
}

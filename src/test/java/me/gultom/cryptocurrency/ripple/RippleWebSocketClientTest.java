package me.gultom.cryptocurrency.ripple;

import com.fasterxml.jackson.databind.JsonNode;
import me.gultom.cryptocurrency.Network;
import org.java_websocket.handshake.ServerHandshake;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.Arrays;

class TestClient extends RippleWebSocketClient {

	private String addressFrom;
	private String addressFromSecret;
	private String addressTo;
	private BigDecimal xrpAmount;
	private Integer fee;

	public TestClient(Network network) throws URISyntaxException {
		super(network);

		// test data
		this.addressFromSecret = "sscGc1S9kpfo3StA7AzQDpNrUZrGU";
		this.addressTo = "rJQjJCDaNRKS8gMES8qcHT1kWWQfrjVdCN";
		xrpAmount = new BigDecimal(1); // XRP
		this.fee = 10000; // drops
	}

	@Override
	public void onOpen(ServerHandshake handshakedata) {
		this.addressFrom = this.getAccountID(this.addressFromSecret);
		// get account info from addressFrom
		this.send(this.getAccountInfoPayload(this.addressFrom));
		// subscribe to addressTo update
		this.send(this.getSubscribeAccountsPayload(Arrays.asList(this.addressTo)));
	}

	@Override
	public void onMessage(String message) {
		try {
			JsonNode body = this.mapper.readTree(message);

			if (body.has("error_message")) {
				throw new RuntimeException(body.get("error_message").textValue());
			}

			// process account data
			if (body.has("result") && body.get("result").has("account_data")) {

				// balance
				if (body.has("result")
						&& body.get("result").has("account_data")
						&& body.get("result").get("account_data").has("Balance")) {
					BigDecimal balance = this.getBalanceFromAccountInfoResponse(body);
					assert balance != null;
					System.out.println("Balance: " + balance);
				}

				// sequence
				if (body.has("result")
						&& body.get("result").has("account_data")
						&& body.get("result").get("account_data").has("Sequence")) {
					Integer sequence = this.getSequenceFromAccountInfoResponse(body);
					assert sequence != null;
					System.out.println("Sequence: " + sequence);

					// create signed transaction
					String txBlob = this.sign(this.xrpAmount, this.addressFrom, this.addressFromSecret, this.addressTo, sequence, this.fee);

					// submit transaction
					this.send(this.getSubmitTransactionPayload(txBlob));
				}

			}

			// process submit transaction response
			else if (body.has("status")
					&& body.has("result")
					&& body.has("type")
					&& body.get("type").asText().equalsIgnoreCase("response")
					&& body.get("result").has("engine_result_code")) {
				if ("success".equalsIgnoreCase(body.get("status").textValue())
						&& body.get("result").get("engine_result_code").asInt() == 0) {
					System.out.println("Transaction success");
					System.out.println(body.toString());
					assert true;
				} else {
					int code = body.get("result").get("engine_result_code").asInt();
					throw new RuntimeException("Transaction failed without error message. Code: " +  code);
				}
			}

			// process transaction update
			else if (body.has("transaction")
				&& body.get("transaction").has("TransactionType")
				&& body.get("transaction").has("Amount")
				&& body.get("transaction").has("date")
				&& body.get("transaction").has("hash")) {
				String receivedAmount = body.get("transaction").get("Amount").asText();
				System.out.println("Amount (drops) received: " + receivedAmount);
				// Date in "Ripple Epoch" of January 1, 2000 (00:00 UTC)
				// This is like the way the Unix epoch works, except the Ripple Epoch is 946684800 seconds after the Unix Epoch.
				System.out.println("Date: " + body.get("transaction").get("date").asLong());
				System.out.println(body.toString());
				this.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}
	}

	@Override
	public void onClose(int code, String reason, boolean remote) {
		System.out.println("Good bye");
	}

	@Override
	public void onError(Exception ex) {
		ex.printStackTrace();
		assert false;
	}
}

public class RippleWebSocketClientTest {

	@Ignore
	@Test
	public void test() {
		try {
			TestClient client = new TestClient(Network.TEST);
			client.connect();
			Thread.sleep(20000);
			assert client.isClosed();
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}
	}
}

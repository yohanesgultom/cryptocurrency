package me.gultom.cryptocurrency.ripple;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import me.gultom.cryptocurrency.Network;
import me.gultom.cryptocurrency.Result;
import me.gultom.cryptocurrency.exception.APIErrorException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

public class RippleClientTest {

//    previously generated address & key pair
//    Public Address: rUQA2cpddviSQDCGogUAgUDFqiWfBNhb8e
//    Private Key: sscGc1S9kpfo3StA7AzQDpNrUZrGU
//    Public Address: rJQjJCDaNRKS8gMES8qcHT1kWWQfrjVdCN
//    Private Key: spuh9B3m5Qg8h4VijprYz1W6tY2Mm

//    generated using https://ripple.com/build/xrp-test-net/
//    10000 XRP
//    Address: rabtKLoKA98PKweXiswLUERyGP4D4pb6q4
//    Secret: shj1bDgcUP69fk7YjCjGwcfXqq6xm

    private RippleClient rippleClient;

    @Before
    public void setUp() throws URISyntaxException {
        this.rippleClient = new RippleClient(Network.TEST);
    }

    @Ignore
    @Test
    public void test_getBalance() {
        String address = "rJQjJCDaNRKS8gMES8qcHT1kWWQfrjVdCN";
        try {
            BigDecimal balance = this.rippleClient.getBalance(address);
            System.out.println(balance.toPlainString());
            assert balance != null;
        } catch (APIErrorException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Ignore
    @Test
    public void test_getAccountInfo() {
        String address = "rJQjJCDaNRKS8gMES8qcHT1kWWQfrjVdCN";
        try {
            JsonNode info = this.rippleClient.getAccountInfo(address);
            assert info.get("status").textValue().equalsIgnoreCase("success");
        } catch (APIErrorException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Ignore
    @Test
    public void test_signLocal() {
        // Minimum reserve per 28 Feb 2018 https://ripple.com/build/reserves/
        BigDecimal xrpAmount = new BigDecimal(20);
        String accountId = "rabtKLoKA98PKweXiswLUERyGP4D4pb6q4";
        String secret = "shj1bDgcUP69fk7YjCjGwcfXqq6xm";
        String address = "rJQjJCDaNRKS8gMES8qcHT1kWWQfrjVdCN";
        try {
            JsonNode accountInfo = this.rippleClient.getAccountInfo(accountId);
            int sequence = accountInfo.get("result").get("account_data").get("Sequence").intValue();
            int fee = 10000;
            String localBlob = this.rippleClient.signLocal(xrpAmount, accountId, secret, address, sequence, fee);
            System.out.println(localBlob);
            assert localBlob != null;
        } catch (APIErrorException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Deprecated
    @Ignore
    @Test
    public void test_sign() {
        // Minimum reserve per 28 Feb 2018 https://ripple.com/build/reserves/
        BigDecimal xrpAmount = new BigDecimal(20);
        String accountId = "rabtKLoKA98PKweXiswLUERyGP4D4pb6q4";
        String secret = "shj1bDgcUP69fk7YjCjGwcfXqq6xm";
        String address = "rJQjJCDaNRKS8gMES8qcHT1kWWQfrjVdCN";
        try {
            Result result = this.rippleClient.sign(xrpAmount, accountId, secret, address);
            if (!result.isSuccess()) {
                for (String error:result.getErrors()) {
                    System.err.println(error);
                }
            }
            assert result.isSuccess();
            System.out.println(result.getBody().toString());
        } catch (APIErrorException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Ignore
    @Test
    public void test_send() {
        // Minimum reserve per 28 Feb 2018 https://ripple.com/build/reserves/
        BigDecimal xrpAmount = new BigDecimal(1);
        String secret = "sscGc1S9kpfo3StA7AzQDpNrUZrGU";
        String address = "rJQjJCDaNRKS8gMES8qcHT1kWWQfrjVdCN";
        try {
            Result result = this.rippleClient.send(xrpAmount, secret, address);
            assert result.isSuccess();
            System.out.println(result.getBody().toString());
        } catch (APIErrorException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Ignore
    @Test
    public void test_subscribe() {
        BigDecimal xrpAmount = new BigDecimal(0.5);
        Integer dropAmount = Math.round(xrpAmount.floatValue() * 1000000);
        String secret = "shj1bDgcUP69fk7YjCjGwcfXqq6xm";
        String address = "rJQjJCDaNRKS8gMES8qcHT1kWWQfrjVdCN";
        List<String> accountList = Arrays.asList(address);

        // example of event message handler
        MessageEventHandler messageEventHandler = (message, client) -> {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                System.out.println(message);
                JsonNode body = objectMapper.readTree(message);
                if (body.has("transaction")) {
                    assert body.get("transaction").get("TransactionType").asText().equalsIgnoreCase("Payment");
                    // Amount is in Drops, where 1 Drops = 0.000001 XRP
                    assert body.get("transaction").get("Amount").asText().equalsIgnoreCase(dropAmount.toString());
                    // Date in "Ripple Epoch" of January 1, 2000 (00:00 UTC)
                    // This is like the way the Unix epoch works, except the Ripple Epoch is 946684800 seconds after the Unix Epoch.
                    System.out.println("date: " + body.get("transaction").get("date").asLong());
                    // consider saving hash for reference
                    System.out.println("hash: " + body.get("transaction").get("hash").asText());
                    // consider checking status and validation status
                    System.out.println("status: " + body.get("status").asText());
                    System.out.println("validated: " + body.get("validated").asBoolean());
                    client.close();
                } else if (!body.get("status").asText().equalsIgnoreCase("success")) {
                    throw new Exception("Failed response");
                }
            } catch (Exception e) {
                e.printStackTrace();
                assert false;
                client.close();
            }
        };

        // example of close handler
        CloseEventHandler closeEventHandler = (code, reason, remote) -> {
            System.out.println("Connection closed. Reason: " + reason);
        };

        // example of error handler
        ErrorEventHandler errorEventHandler = (e, client) -> {
            e.printStackTrace();
            client.close();
            assert false;
        };

        try {
            // start listening
            this.rippleClient.subscribe(accountList, messageEventHandler, closeEventHandler, errorEventHandler);

            // send some amount to trigger event
            Result result = this.rippleClient.send(xrpAmount, secret, address);
            assert result.isSuccess();
            assert true;

            // wait for event
            Thread.sleep(10000);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            assert false;
        } catch (APIErrorException e) {
            e.printStackTrace();
            assert false;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

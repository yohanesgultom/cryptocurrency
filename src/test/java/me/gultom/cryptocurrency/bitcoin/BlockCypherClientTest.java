package me.gultom.cryptocurrency.bitcoin;

import me.gultom.cryptocurrency.Network;
import me.gultom.cryptocurrency.Result;
import me.gultom.cryptocurrency.exception.APIErrorException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;

public class BlockCypherClientTest {

    private BlockCypherClient blockCypherClient;

    @Before
    public void setUp() {
        String token = "c769168d30e74f469b52ee106fb16a78";
        this.blockCypherClient = new BlockCypherClient(Network.TEST, token);
    }

    @Ignore
    @Test
    public void test_getBalance() {
        String address = "n4FyuAzjSKgAXgFP7zViarWDtu8TXJCrTZ";
        try {
            BigDecimal balance = this.blockCypherClient.getBalance(address);
            System.out.println("Balance: " + balance.toString());
            assert balance != null;
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        } catch (APIErrorException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Test
    public void test_send() {
        String privateKey = "cTFHKfjan9xBmL1SH7eeEo3zJgjRkrL7WhWuR5bVoVTLWjADi6JY";
        String addressTo = "mtDANiVM44PLupteTaBojzWPGVkRGyseG9";
        BigDecimal btcAmount = new BigDecimal(0.01);
        try {
            Result result = this.blockCypherClient.send(btcAmount, privateKey, addressTo);
            if (!result.isSuccess() && result.getErrors().length > 0) {
                for (String error : result.getErrors()) {
                    System.err.println(error);
                }
            }
            assert result.isSuccess();
            System.out.println("Tx hash: " + result.getBody().get("tx").get("hash").textValue());
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Ignore
    @Test
    public void test_registerWebhook() {
        String address = "mtDANiVM44PLupteTaBojzWPGVkRGyseG9";
        String callbackURL = "https://nasigoreng.herokuapp.com/pmupj9pm";
        try {
            // use BlockCypherClient.Event.CONFIRMEDTX to listen for confirmed event only
            Result result = this.blockCypherClient.registerWebhook(BlockCypherClient.Event.UNCONFIRMEDTX, address, callbackURL);
            if (!result.isSuccess() && result.getErrors().length > 0) {
                for (String error : result.getErrors()) {
                    System.err.println(error);
                }
            }
            assert result.isSuccess();
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Test
    public void test_getReceivedAmountFromTX() {
        String txJson = "{\"block_height\":-1,\"block_index\":-1,\"hash\":\"eb3ee4f7490dfcdc5f48336661cb401419ef8da6e273ba59a2958dbfc3a594b1\",\"addresses\":[\"mtDANiVM44PLupteTaBojzWPGVkRGyseG9\",\"n4FyuAzjSKgAXgFP7zViarWDtu8TXJCrTZ\"],\"total\":65889800,\"fees\":10300,\"size\":225,\"preference\":\"low\",\"relayed_by\":\"125.165.137.63\",\"received\":\"2018-07-21T10:46:07.06Z\",\"ver\":1,\"double_spend\":false,\"vin_sz\":1,\"vout_sz\":2,\"confirmations\":0,\"inputs\":[{\"prev_hash\":\"4923394cf2ddba30e0d82bb601ad4f0acc02e106af9d1cc7700353bb93013d65\",\"output_index\":0,\"script\":\"47304402205005b6bb546f381301dfb6f04ecd5019c6a3e4e377176e362874f2585b08d36002200153c88e8c82d00e2ed8588c6d8d914ddada8c5087173852cbe60b39d7821538012103186a5acacbf165df5247a8c586bb0f97e958710f5522b65f29ac93a6d23ba404\",\"output_value\":65900100,\"sequence\":4294967295,\"addresses\":[\"n4FyuAzjSKgAXgFP7zViarWDtu8TXJCrTZ\"],\"script_type\":\"pay-to-pubkey-hash\",\"age\":1253566}],\"outputs\":[{\"value\":1000000,\"script\":\"76a9148b3c3b69084e3933af90dd9894d202833de24d3688ac\",\"addresses\":[\"mtDANiVM44PLupteTaBojzWPGVkRGyseG9\"],\"script_type\":\"pay-to-pubkey-hash\"},{\"value\":64889800,\"script\":\"76a914f975fe6a0ee7d8ba90cea0f9ad0b9a3c41bd974688ac\",\"addresses\":[\"n4FyuAzjSKgAXgFP7zViarWDtu8TXJCrTZ\"],\"script_type\":\"pay-to-pubkey-hash\"}]}";
        String address = "mtDANiVM44PLupteTaBojzWPGVkRGyseG9";
        try {
            BigDecimal amount = this.blockCypherClient.getReceivedAmountFromTX(txJson, address);
            assert amount.doubleValue() == 0.01;
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Ignore
    @Test
    public void test_listWebhooks() {
        try {
            Result result = this.blockCypherClient.listWebhooks();
            if (!result.isSuccess() && result.getErrors().length > 0) {
                for (String error : result.getErrors()) {
                    System.err.println(error);
                }
            }
            assert result.isSuccess();
            System.out.println(result.getBody().toString());
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Ignore
    @Test
    public void test_getWebhook() {
        String id = "1069ed26-16e5-44b8-a99d-1f09a205eb81";
        try {
            Result result = this.blockCypherClient.getWebhook(id);
            if (!result.isSuccess() && result.getErrors().length > 0) {
                for (String error : result.getErrors()) {
                    System.err.println(error);
                }
                System.err.println(result.getCode());
            }
            assert result.isSuccess();
            System.out.println(result.getBody().toString());
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Ignore
    @Test
    public void test_deleteWebhook() {
        String id = "c6541329-b94f-414e-a6ad-7ccbb25ddf29";
        try {
            Result result = this.blockCypherClient.deleteWebhook(id);
            if (!result.isSuccess() && result.getErrors().length > 0) {
                for (String error : result.getErrors()) {
                    System.err.println(error);
                }
            }
            assert result.isSuccess();
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
    }
}

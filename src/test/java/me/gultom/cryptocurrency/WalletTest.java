package me.gultom.cryptocurrency;

import me.gultom.cryptocurrency.bitcoin.BitcoinWallet;
import me.gultom.cryptocurrency.bitcoin.BitcoinWalletFactory;
import me.gultom.cryptocurrency.ethereum.EthereumWallet;
import me.gultom.cryptocurrency.ethereum.EthereumWalletFactory;
import me.gultom.cryptocurrency.exception.APIErrorException;
import me.gultom.cryptocurrency.ripple.RippleWallet;
import me.gultom.cryptocurrency.ripple.RippleWalletFactory;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;

public class WalletTest {

    /**
     * Generate new random wallet
     */
    @Test
    public void test_factory() {
        try {
            WalletFactory factory = new BitcoinWalletFactory();
            Wallet wallet = factory.generate(Network.TEST);
            assert wallet.getClass().equals(BitcoinWallet.class);
            assert wallet.getPrivateKey().length() > 10;
            assert wallet.getPublicAddress().length() > 10;
            assert wallet.isValid();

            factory = new EthereumWalletFactory();
            wallet = factory.generate(Network.TEST);
            assert wallet.getClass().equals(EthereumWallet.class);
            assert wallet.getPrivateKey().length() > 10;
            assert wallet.getPublicAddress().length() > 10;
            assert wallet.isValid();

            factory = new RippleWalletFactory();
            wallet = factory.generate(Network.TEST);
            assert wallet.getClass().equals(RippleWallet.class);
            assert wallet.getPrivateKey().length() > 10;
            assert wallet.getPublicAddress().length() > 10;
            assert wallet.isValid();

        } catch (Exception e) {
            assert false;
        }
    }

    /**
     * Generate new wallet and check balance
     * Except for Ripple that only allow balance check on existing account
     */
    @Ignore
    @Test
    public void test_getBalance() {
        WalletFactory factory = new BitcoinWalletFactory();
        Wallet wallet = factory.generate(Network.TEST);
        ApiConfig config = new ApiConfig("c769168d30e74f469b52ee106fb16a78", null);
        try {
            BigDecimal balance = wallet.getBalance(config);
            assert balance.equals(BigDecimal.ZERO);
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        } catch (APIErrorException e) {
            e.printStackTrace();
            assert false;
        }

        factory = new EthereumWalletFactory();
        wallet = factory.generate(Network.TEST);
        config = new ApiConfig("U7AMNQVK3F5G9BBWGW1QQZS19D7GK58HJ8", null);
        try {
            BigDecimal balance = wallet.getBalance(config);
            assert balance.equals(BigDecimal.ZERO);
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        } catch (APIErrorException e) {
            e.printStackTrace();
            assert false;
        }

        // Ripple account is only active after receiving XRP as much as minimum reserves https://ripple.com/build/reserves/
        // So we can only check existing account
        factory = new RippleWalletFactory();
        String address = "rUQA2cpddviSQDCGogUAgUDFqiWfBNhb8e";
        String key = "sscGc1S9kpfo3StA7AzQDpNrUZrGU";
        wallet = factory.create(address, key, Network.TEST);
        // not using third party API
        config = new ApiConfig(null, null);
        try {
            BigDecimal balance = wallet.getBalance(config);
            assert balance != null;
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        } catch (APIErrorException e) {
            e.printStackTrace();
            assert false;
        }
    }

    /**
     * Test send coins using address and key that are
     * previously generated using this library
     */
    @Ignore
    @Test
    public void test_btc_send() {
        String address = "mtDANiVM44PLupteTaBojzWPGVkRGyseG9";
        String key = "cVsc4qj3nbFV5NSfVPTmV2pkzWuwoA62M2s2srdszYKEBU7RxUkt";
        String to = "n4FyuAzjSKgAXgFP7zViarWDtu8TXJCrTZ";
        WalletFactory factory = new BitcoinWalletFactory();
        Wallet wallet = factory.create(address, key, Network.TEST);
        ApiConfig config = new ApiConfig("c769168d30e74f469b52ee106fb16a78", null);
        BigDecimal amount = new BigDecimal(0.01);
        try {
            Result result = wallet.send(amount, to, config);
            if (!result.isSuccess()) {
                for (String error:result.getErrors()) {
                    System.err.println(error);
                }
            }
            assert result.isSuccess();
            System.out.println("Tx hash: " + result.getBody().get("tx").get("hash").textValue());
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        } catch (APIErrorException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Ignore
    @Test
    public void test_eth_send() {
        String address = "0x52ebf5732727fcd480c74433f433e92a77235600";
        String key = "b6b992279f38acad007d87d19f15e28b39422c38af677ea92970e7c4ccdff63c";
        String to = "0x46467CA9FfE3E8A5eB9BfB8f50797A324BB93573";
        WalletFactory factory = new EthereumWalletFactory();
        Wallet wallet = factory.create(address, key, Network.TEST);
        ApiConfig config = new ApiConfig("c769168d30e74f469b52ee106fb16a78", null);
        BigDecimal amount = new BigDecimal(0.01);
        try {
            Result result = wallet.send(amount, to, config);
            if (!result.isSuccess()) {
                for (String error:result.getErrors()) {
                    System.err.println(error);
                }
            }
            assert result.isSuccess();
            System.out.println("Tx hash: " + result.getBody().get("result"));
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        } catch (APIErrorException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Ignore
    @Test
    public void test_xrp_send() {
        String address = "rUQA2cpddviSQDCGogUAgUDFqiWfBNhb8e";
        String key = "sscGc1S9kpfo3StA7AzQDpNrUZrGU";
        String to = "rJQjJCDaNRKS8gMES8qcHT1kWWQfrjVdCN";
        WalletFactory factory = new RippleWalletFactory();
        Wallet wallet = factory.create(address, key, Network.TEST);
        // not using third party API
        ApiConfig config = new ApiConfig(null, null);
        BigDecimal amount = new BigDecimal(1);
        try {
            Result result = wallet.send(amount, to, config);
            if (!result.isSuccess()) {
                for (String error:result.getErrors()) {
                    System.err.println(error);
                }
            }
            assert result.isSuccess();
            System.out.println("Tx hash: " + result.getBody().get("result"));
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        } catch (APIErrorException e) {
            e.printStackTrace();
            assert false;
        }

    }
}

package me.gultom.cryptocurrency;

import me.gultom.cryptocurrency.bitcoin.BitcoinHDWallet;
import me.gultom.cryptocurrency.bitcoin.BitcoinHDWalletFactory;
import me.gultom.cryptocurrency.ethereum.EthereumHDWallet;
import me.gultom.cryptocurrency.ethereum.EthereumHDWalletFactory;
import org.junit.Test;

public class HDWalletTest {

    @Test
    public void test_BitcoinHDWalletFactory() {
        try {
            // generate new wallet
            BitcoinHDWalletFactory factory = new BitcoinHDWalletFactory();
            BitcoinHDWallet wallet = (BitcoinHDWallet) factory.generate(Network.TEST);
            String address0 = wallet.getPublicAddress();
            String address1 = wallet.getFreshPublicAddress();
            String address2 = wallet.getFreshPublicAddress();
            String address3 = wallet.getFreshPublicAddress();
            assert wallet.getClass().equals(BitcoinHDWallet.class);
            assert wallet.getPublicAddress().equals(address3);
            assert !wallet.getPublicAddress().equals(address1);
            assert !wallet.getPublicAddress().equals(address2);
            assert !address1.equals(address2);
            assert wallet.getCounter() == 3;

            // get private key by public address
            String key = wallet.getPrivateKeyByAddress(wallet.getPublicAddress());
            assert wallet.getPrivateKey().equals(key);

            // test restoration without counter (restart keychain)
            BitcoinHDWallet restoredWallet = (BitcoinHDWallet)factory.create(
                    wallet.getMnemonicCode(),
                    wallet.getCreationTime(),
                    0,
                    Network.TEST
            );
            assert restoredWallet.getMnemonicCode().equals(wallet.getMnemonicCode());
            assert restoredWallet.getPublicAddress().equals(address0);
            assert restoredWallet.getFreshPublicAddress().equals(address1);
            assert restoredWallet.getFreshPublicAddress().equals(address2);
            assert restoredWallet.getFreshPublicAddress().equals(address3);

            // get private key by public address
            String restoredKey = restoredWallet.getPrivateKeyByAddress(wallet.getPublicAddress());
            assert wallet.getPrivateKey().equals(restoredKey);

            // test restoration
            BitcoinHDWallet restoredWallet2 = (BitcoinHDWallet)factory.create(
                    wallet.getMnemonicCode(),
                    wallet.getCreationTime(),
                    wallet.getCounter(),
                    Network.TEST
            );

            assert restoredWallet2.getPublicAddress().equals(wallet.getPublicAddress());

        } catch (Exception e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Test
    public void test_EthereumHDWalletFactory() {
        try {
            // generate new wallet
            EthereumHDWalletFactory factory = new EthereumHDWalletFactory();
            EthereumHDWallet wallet = (EthereumHDWallet) factory.generate();
            assert wallet.getClass().equals(EthereumHDWallet.class);
            assert wallet.getPrivateKey() != null;
            assert wallet.getPublicAddress() != null;
            assert wallet.getMnemonicCode() != null;
            System.out.println(wallet.getPublicAddress());
            System.out.println(wallet.getPrivateKey());

            // test restoration
            EthereumHDWallet restoredWallet = (EthereumHDWallet) factory.create(wallet.getMnemonicCode());
            assert restoredWallet.getPublicAddress().equals(wallet.getPublicAddress());
            assert restoredWallet.getPrivateKey().equals(wallet.getPrivateKey());
            assert restoredWallet.getMnemonicCode().equals(wallet.getMnemonicCode());

        } catch (Exception e) {
            e.printStackTrace();
            assert false;
        }
    }
}

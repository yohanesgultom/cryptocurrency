package me.gultom.cryptocurrency.ethereum;

import me.gultom.cryptocurrency.Network;
import me.gultom.cryptocurrency.Result;
import me.gultom.cryptocurrency.exception.APIErrorException;
import org.ethereum.core.Denomination;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

public class EtherscanClientTest {

    private EtherscanClient etherscanClient;

    @Before
    public void setUp() {
        String token = "U7AMNQVK3F5G9BBWGW1QQZS19D7GK58HJ8";
        this.etherscanClient = new EtherscanClient(Network.TEST, token);
    }

    @Ignore
    @Test
    public void test_getGasPrice() {
        try {
            Result result = this.etherscanClient.getGasPrice();
            String gasPriceHex = result.getBody().get("result").textValue();
            byte[] gasPrice = new BigInteger(gasPriceHex.substring(2), 16).toByteArray();
            assert gasPrice.length > 0;
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Ignore
    @Test
    public void test_getBalance() {
        try {
            String address = "0xB42b8f0aac75c7231AF3d9403cCFBE667892d2Cf";
            BigDecimal balance = this.etherscanClient.getBalance(address);
            assert balance != null;
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        } catch (APIErrorException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Ignore
    @Test
    public void test_getEstimateGas() {
        try {
            String addressTo = "0x893BA6a4Edd7209d3CC865e1a74D3b54ec657046";
            BigDecimal ethAmount = new BigDecimal(0.01);
            BigInteger weiAmount = ethAmount
                    .multiply(new BigDecimal(Denomination.ETHER.value()))
                    .toBigInteger();
            Result result = this.etherscanClient.getGasPrice();
            String gasPriceHex = result.getBody().get("result").textValue();
            result = this.etherscanClient.getEstimateGas(addressTo, weiAmount, gasPriceHex);
            String gasHex = result.getBody().get("result").textValue();
            byte[] gas = new BigInteger(gasHex.substring(2), 16).toByteArray();
            assert gas.length > 0;
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Ignore
    @Test
    public void test_getTransactionCount() {
        try {
            String address = "0x46467CA9FfE3E8A5eB9BfB8f50797A324BB93573";
            Result result = this.etherscanClient.getTransactionCount(address);
            String countHex = result.getBody().get("result").textValue();
//            System.out.println(new BigInteger(countHex.substring(2), 16).intValue());
            byte[] count = new BigInteger(countHex.substring(2), 16).toByteArray();
            assert count.length > 0;
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Ignore
    @Test
    public void test_getSend() {
        String privateKey = "216e4970224505f11763047df468e31f3ab6d1bf289d0eddd90cb380176517a0";
        String addressTo = "0x52ebf5732727fcd480c74433f433e92a77235600";
        BigDecimal ethAmount = new BigDecimal(0.1);
        try {
            Result result = this.etherscanClient.send(ethAmount, privateKey, addressTo);
            if (!result.isSuccess() && result.getErrors().length > 0) {
                for (String error : result.getErrors()) {
                    System.err.println(error);
                }
            }
            assert result.isSuccess();
            System.out.println("Tx hash: " + result.getBody().get("result"));
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
    }
}

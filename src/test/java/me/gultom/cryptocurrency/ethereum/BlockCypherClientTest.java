package me.gultom.cryptocurrency.ethereum;

import me.gultom.cryptocurrency.Network;
import me.gultom.cryptocurrency.Result;
import me.gultom.cryptocurrency.exception.APIErrorException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;

public class BlockCypherClientTest {
	private BlockCypherClient blockCypherClient;

	@Before
	public void setUp() {
		String token = "c769168d30e74f469b52ee106fb16a78";
		this.blockCypherClient = new BlockCypherClient(Network.TEST, token);
	}

	@Ignore
	@Test
	public void test_getBalance() {
		// private: 1a3d6332aa4352a4ecb71f66dc333aa5247b740a5f3eb782cf8c11ecd851ecab
		String address = "451d75266e9f12f49f36e4eb37d611158400ce1f";
		try {
			BigDecimal balance = this.blockCypherClient.getBalance(address);
			System.out.println("Balance: " + balance.toString());
			assert balance != null;
		} catch (IOException e) {
			e.printStackTrace();
			assert false;
		} catch (APIErrorException e) {
			e.printStackTrace();
			assert false;
		}
	}

	@Ignore
	@Test
	public void test_send() {
		String privateKey = "1a3d6332aa4352a4ecb71f66dc333aa5247b740a5f3eb782cf8c11ecd851ecab";
		String addressTo = "6acE5827D8c05b6b39485Ba9EB8924E9BDA524e4";
		BigDecimal ethAmount = new BigDecimal(0.12);
		try {
			Result result = this.blockCypherClient.send(ethAmount, privateKey, addressTo);
			if (!result.isSuccess() && result.getErrors().length > 0) {
				for (String error : result.getErrors()) {
					System.err.println(error);
				}
			}
			assert result.isSuccess();
			System.out.println("Tx hash: " + result.getBody().get("tx").get("hash").textValue());
		} catch (IOException e) {
			e.printStackTrace();
			assert false;
		}
	}

	@Ignore
	@Test
	public void test_registerWebhook() {
		String address = "d5798492263b7f9f2c527c1d9042fc154b0afe22";
		String callbackURL = "https://nasigoreng.herokuapp.com/1e0sekp1";
		try {
			// use BlockCypherClient.Event.CONFIRMEDTX to listen for confirmed event only
			Result result = this.blockCypherClient.registerWebhook(BlockCypherClient.Event.UNCONFIRMEDTX, address, callbackURL);
			if (!result.isSuccess() && result.getErrors().length > 0) {
				for (String error : result.getErrors()) {
					System.err.println(error);
				}
			}
			assert result.isSuccess();
		} catch (IOException e) {
			e.printStackTrace();
			assert false;
		}
	}

	@Ignore
	@Test
	public void test_getReceivedAmountFromTX() {
		// private: 7f720dfd384a287af2bac22982c6874914087cade1ef752ee0a1b829ed95b3e7
		String txJson = "{\"block_height\":-1,\"block_index\":0,\"hash\":\"e8f64de7d6868a12cdb8429ccd6ee8d53c42095b91cf57d44f72715e7139242a\",\"addresses\":[\"451d75266e9f12f49f36e4eb37d611158400ce1f\",\"d5798492263b7f9f2c527c1d9042fc154b0afe22\"],\"total\":10000000000000000,\"fees\":672000000000000,\"size\":109,\"gas_limit\":21000,\"gas_price\":32000000000,\"relayed_by\":\"125.165.137.63\",\"received\":\"2018-07-22T14:25:53.338Z\",\"ver\":0,\"double_spend\":false,\"vin_sz\":1,\"vout_sz\":1,\"confirmations\":0,\"inputs\":[{\"sequence\":1,\"addresses\":[\"451d75266e9f12f49f36e4eb37d611158400ce1f\"]}],\"outputs\":[{\"value\":10000000000000000,\"addresses\":[\"d5798492263b7f9f2c527c1d9042fc154b0afe22\"]}]}";
		String address = "d5798492263b7f9f2c527c1d9042fc154b0afe22";
		try {
			BigDecimal amount = this.blockCypherClient.getReceivedAmountFromTX(txJson, address);
			assert amount.doubleValue() == 0.01;
		} catch (IOException e) {
			e.printStackTrace();
			assert false;
		}
	}
}

# Cryptocurrency Wrapper

Wrapper for cryptocurrency blockhain libraries. Currently supporting Bitcoin, Ethereum and Ripple.

## Dependencies

Requirement to run, test and build:

* Java >= 11
* Gradle >= 5 (only for test and build)

## Test

* Run unit tests `gradle clean test`

> All methods calling third parties will not be invoked by default to avoid wasting resources. To run them, please manually remove the `@Ignore` annotation from each test 

## Build

Build with Gradle:

* Without dependencies: `gradle jar`
* With dependencies (fat): `gradle fatJar`

The `jar` will be generated in `dist` directory:

* `blockchain-{version}.jar` (without dependencies)
* `blockchain-all-{version}.jar` (with dependencies)

> JAR without dependencies is useful when one needs to avoid duplicate/multiversion library in a project. To do so, copy dependencies from `build.gradle` which is not yet available in the project.

Build with Maven:

Find `ripple` jars from `lib/` or build from [source](https://github.com/ripple/ripple-lib-java). Install them to local repo:

```
mvn install:install-file -Dfile=lib/ripple-bouncycastle-0.0.1-SNAPSHOT.jar -DgroupId=com.ripple -DartifactId=ripple-bouncycastle -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar
mvn install:install-file -Dfile=lib/ripple-core-0.0.1-SNAPSHOT.jar -DgroupId=com.ripple -DartifactId=ripple-core -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar
```

Run build `mvn package` and get the jar from `target/blockchain-1.0-SNAPSHOT.jar`

## Third Party API

This wrapper uses third party library to connect to blockchain network. Register to each service to obtain the API key/token:

1. [BlockCypher](https://www.blockcypher.com/) (for Bitcoin)
2. [Etherscan](https://etherscan.io/) (for Ethereum)

> Ripple does not need third party API as it has its own websocket api servers

## Usage

The main class is `Wallet` that allows us to check and send balance. Currently supported use-cases are:

1. Generate new wallet using `WalletFactory.generate()`
1. Import existing keys `WalletFactory.create()`
1. Get wallet balance using `Wallet.getBalance()`
1. Send balance (coins) using `Wallet.send()`

Complete code examples can be found in `src/test/java/me/gultom/cryptocurrency/WalletTest.java`.